package com.beautycoder.pflockscreen;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.view.View;

import java.io.Serializable;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by Aleksandr Nikiforov on 2018/02/14.
 */
public class PFFLockScreenConfiguration {

    private String mLeftButton = "";
    private static View.OnClickListener mOnLeftButtonClickListener = null;
    private boolean mUseFingerprint = false;
    private String mTitle = "";
    private int mMode = MODE_AUTH;
    private int mCodeLength = 4;

    private PFFLockScreenConfiguration(Builder builder) {
        mLeftButton = builder.mLeftButton;
        mUseFingerprint = builder.mUseFingerprint;
        mTitle = builder.mTitle;
        //mOnLeftButtonClickListener = builder.mOnLeftButtonClickListener;
        mMode = builder.mMode;
        mCodeLength = builder.mCodeLength;
    }

    public String getLeftButton() {
        return mLeftButton;
    }

    public boolean isUseFingerprint() {
        return mUseFingerprint;
    }

    public String getTitle() {
        return mTitle;
    }

    public View.OnClickListener getOnLeftButtonClickListener() {
        return mOnLeftButtonClickListener;
    }


    public int getCodeLength() {
        return mCodeLength;
    }

    public @PFLockScreenMode int getMode() {
        return this.mMode;
    }

    public static class Builder implements Parcelable {

        private String mLeftButton = "";
        //private View.OnClickListener mOnLeftButtonClickListener = null;
        private boolean mUseFingerprint = false;
        private String mTitle = "";
        private int mMode = 0;
        private int mCodeLength = 4;


        public Builder(View.OnClickListener onClickListener, Context context) {
            mTitle = context.getResources().getString(R.string.lock_screen_title_pf);
            mOnLeftButtonClickListener = onClickListener;
        }

        public Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public Builder setLeftButton(String leftButton) {
            mLeftButton = leftButton;

            return this;
        }

        public Builder setUseFingerprint(boolean useFingerprint) {
            mUseFingerprint = useFingerprint;
            return this;
        }

        public Builder setMode(@PFLockScreenMode int mode) {
            mMode = mode;
            return this;
        }

        public Builder setCodeLength(int codeLength) {
            this.mCodeLength = codeLength;
            return this;
        }

        public PFFLockScreenConfiguration build() {
            return new PFFLockScreenConfiguration(
                    this);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.mLeftButton);
            //dest.writeParcelable((Parcelable) this.mOnLeftButtonClickListener, PARCELABLE_WRITE_RETURN_VALUE);
            dest.writeByte(this.mUseFingerprint ? (byte) 1 : (byte) 0);
            dest.writeString(this.mTitle);
            dest.writeInt(this.mMode);
            dest.writeInt(this.mCodeLength);
        }

        protected Builder(Parcel in) {
            this.mLeftButton = in.readString();
            //this.mOnLeftButtonClickListener = in.readParcelable(View.OnClickListener.class.getClassLoader());
            this.mUseFingerprint = in.readByte() != 0;
            this.mTitle = in.readString();
            this.mMode = in.readInt();
            this.mCodeLength = in.readInt();
        }

        public static final Parcelable.Creator<Builder> CREATOR = new Parcelable.Creator<Builder>() {
            @Override
            public Builder createFromParcel(Parcel source) {
                return new Builder(source);
            }

            @Override
            public Builder[] newArray(int size) {
                return new Builder[size];
            }
        };
    }

    @Retention(SOURCE)
    @IntDef({MODE_CREATE, MODE_AUTH})
    public @interface PFLockScreenMode {}
    public static final int MODE_CREATE = 0;
    public static final int MODE_AUTH = 1;

}
