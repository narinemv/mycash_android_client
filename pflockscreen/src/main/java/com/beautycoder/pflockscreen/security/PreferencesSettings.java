package com.beautycoder.pflockscreen.security;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by aleksandr on 2018/02/09.
 */

public class PreferencesSettings {

    private static final String PREF_FILE = "settings_pref";

    public static void saveToPref(Context context, String str) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("code", str);
        editor.apply();
    }

    public static String getCode(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        if( sharedPref != null) {
            return sharedPref.getString("code", "");
        }else{
            return "";
        }
    }

    public static void removePin (Context context) {
        Log.d("TAG", "remove pin");
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("code");
        editor.apply();
    }
}
