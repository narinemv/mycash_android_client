package com.mycash.client;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.StrictMode;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.mycash.client.api.NetworkService;
import com.mycash.client.helper.LocaleManager;
import com.mycash.client.helper.Utils;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Narine on 02,March,2018
 */
public class MyCashClientApplication extends Application{

    public static NetworkService networkService;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
        Log.d(MyCashClientConstants.TAG, "attachBaseContext");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
        Log.d(MyCashClientConstants.TAG, "onConfigurationChanged: " + newConfig.locale.getLanguage());
    }

    @Override
    public void onCreate (){
        super.onCreate();
        //Fabric.with(this, new Crashlytics());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        crateNetworkService();

        //TODO: this check leackCanary lib work for memory leak and must be removed
        //setupLeakCanary();
    }

    public void crateNetworkService() {
        networkService = new NetworkService(Utils.getToken(this));
    }

    //TODO: this check leackCanary lib work for memory leak and must be removed
    protected void setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        //enabledStrictMode();
        LeakCanary.install(this);
    }

    //TODO: this check leackCanary lib work for memory leak and must be removed
    private static void enabledStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder() //
                .detectAll() //
                .penaltyLog() //
                .penaltyDeath() //
                .build());
    }

    public static NetworkService getNetworkService(){
        return networkService;
    }

}