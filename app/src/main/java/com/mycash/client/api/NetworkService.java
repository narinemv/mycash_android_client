package com.mycash.client.api;

import android.util.Log;

import com.mycash.client.BuildConfig;
import com.mycash.client.MyCashClientConstants;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Narine on 04,April,2018
 */
public class NetworkService {

    private String baseUrl = "https://www.mycash.am/";
    private NetworkAPI networkAPI;
    private String token;

    public NetworkService(String token){
        this();
        this.token = token;
    }

    public NetworkService(){
        OkHttpClient okHttpClient = buildClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     * @return
     */
    public NetworkAPI getAPI(){
        return  networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     * @return
     */
    public OkHttpClient buildClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
//        builder.addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Interceptor.Chain chain) throws IOException {
//                Response response = chain.proceed(chain.request());
//                // Do anything with response here
//                // if we ant to grab a specific cookie or something..
//                return response;
//            }
//        });
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                // this is where we will add whatever we want to our request headers.
                Request request = null;

                if(token != null && !token.isEmpty()) {
                   request = chain.request().newBuilder()
                            .addHeader("Accept", "application/json")
                            .addHeader("Authorization",token)
                            .build();
                }else {
                   request = chain.request().newBuilder()
                            .addHeader("Accept", "application/json")
                            .build();
                }

                return chain.proceed(request);
            }
        });

        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);
        builder.writeTimeout(120, TimeUnit.SECONDS);

        return  builder.build();
    }

    public void removeToken (){
        this.token = null;
    }

    public void updateToken (String newToken){
        this.token = MyCashClientConstants.TOKEN_PREFIX + newToken;
    }

    /**
     * Converted error into ResponseBody to get Message
     *
     * @param throwable
     * @return
     */
    public String getErrorMessage(Throwable throwable) {
        try {
            if (throwable != null && throwable instanceof HttpException) {
                ResponseBody body = ((HttpException) throwable).response().errorBody();
                String error = body.string();
                Log.e(MyCashClientConstants.TAG, "::getErrorMessage -> error : " + error);
                if(error != null || !error.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(error);
                    return jsonObject.getString("Message");
                } else {
                    return "";
                }
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
