/**
 * all the Service calls to use for the retrofit requests.
 */
package com.mycash.client.api;

import com.mycash.client.model.entity.AdvertisementResponse;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.model.entity.ChangePasswordRequest;
import com.mycash.client.model.entity.LocationRequest;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.model.entity.PaymentHistoryDetails;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.model.entity.TransferHistoryDetails;
import com.mycash.client.model.entity.User;
import com.mycash.client.model.entity.ValidateRequest;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Narine on 05,April,2018
 */
public interface NetworkAPI {

    @POST("/api/Buyer/Login")
    Observable<LoginResponse> cashierLogin(@Header("Authorization") String token);

    @POST("api/Buyer/ChackBuyerBonus")
    Observable<BuyerBonusResponse> getBranchResponse(@Body Request request);

    @POST("api/Buyer/Payments")
    Observable<List<PaymentHistory>> getPayments();

    @POST("api/Buyer/PaymentDetail/{payment_id}")
    Observable<PaymentHistoryDetails> getPaymentDetails(@Path("payment_id") String paymentId);

    @GET("api/Buyer/Branches")
    Observable<List<BranchResponse>> getBranches();

    @POST("api/Buyer/ChackOtherBuyerState")
    Observable<CheckOtherBuyerStateResponse> CheckOtherBuyerState(@Body Request request);

    @POST("api/Buyer/TransferBonus")
    Observable<TransferBonus> transferBonus(@Body TransferBonus request);

    @POST("api/Buyer/Transfers")
    Observable<List<TransferBonus>> getTrasferHistory();

    @POST("api/Buyer/Register")
    Observable<LoginResponse> registration(@Body User registration);

    @POST("api/Buyer/TransferDetail/{transfer_id}")
    Observable<TransferHistoryDetails> getTrasferHistoryDetails(@Path("transfer_id") String transferId);

    @POST("api/Buyer/Advertisements")
    Observable<List<AdvertisementResponse>> getAdvertisements(@Body LocationRequest locationRequest );

    @POST("api/Buyer/SendCodeToEmail")
    Observable<String> sendCodeToEmail(@Body Request request);

    @POST("api/Buyer/ChangePassword")
    Observable<String> changePassword(@Body ChangePasswordRequest request);

    @POST("api/Buyer/Validate")
    Observable<String> validateBuyer(@Body ValidateRequest request);

    @POST("api/Buyer/AroundBranches/{category_id}")
    Observable<List<BranchResponse>> getBranchesByCategoryId(@Path("category_id") String categoryId, @Body LocationRequest request);
}
