package com.mycash.client.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.model.entity.LoginResponse;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Narine on 6/21/2017.
 */

public class Utils {

	/**
	 *
	 * @param phoneNumber
	 * @return
	 */
	public static boolean isValidPhoneNumber(String phoneNumber) {
		if (TextUtils.isEmpty(phoneNumber)) {
			return false;
		}

		if (phoneNumber.endsWith("-")) {
			return false;
		}
		if (phoneNumber.contains("-")) {
			phoneNumber = phoneNumber.replace("-", "");
		}
		boolean isValid = false;
		final boolean globalPhoneNumber = PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber);
		if (globalPhoneNumber) {
			final boolean matches = phoneNumber.matches("^[+]?[0-9]{7,18}$");
			if (matches) {
				Log.w(MyCashClientConstants.TAG, "addTextChanged - OK");
				isValid = true;
			}
		}
		return isValid;
	}

	/**
	 *
	 * @param dateStr
	 * @return
	 */
	public static String changeDateFormat (String dateStr) {
		String dateTime = null;
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat output = new SimpleDateFormat("yyyy/MM/dd hh:mm");
		Date d = null;
		try {
			d = dateformat.parse(dateStr /*your date as String*/);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		dateTime = output.format(d);

		return dateTime;
	}

	/**
	 * This function saves the token that will be used in all requests
	 *
	 * @param loginResponse
	 * @param context
	 */
	public static void saveData(LoginResponse loginResponse, Context context) {
		SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		String token = loginResponse.getToken();

		if (token != null && !token.isEmpty()) {
			Log.d(MyCashClientConstants.TAG, "authentication token " + token);
			editor.putString(MyCashClientConstants.TOKEN, "Basic " + token);
		}
		editor.putString(MyCashClientConstants.FILE_NAME, loginResponse.getBuyer().getFirstName());
		editor.putString(MyCashClientConstants.LAST_NAME, loginResponse.getBuyer().getLastName());
		editor.putString(MyCashClientConstants.EMAIL, loginResponse.getBuyer().getEmail());
		editor.putString(MyCashClientConstants.PHONE, loginResponse.getBuyer().getPhone());
		editor.putString(MyCashClientConstants.CCC, loginResponse.getBuyer().getCCC());
		editor.putString(MyCashClientConstants.REGISTER_DATE, loginResponse.getBuyer().getRegDate());
		editor.putString(MyCashClientConstants.IMAGES_BASE_URL, loginResponse.getPathToImageFiles());

		editor.apply();
	}

	public static void saveLocation (Location location, Context context) {
		SharedPreferences.Editor editor = getSharedPreferences(context).edit();

		editor.putString(MyCashClientConstants.LATITUDE, (Double.toString(location.getLatitude())));
		editor.putString(MyCashClientConstants.LONGITUDE, (Double.toString(location.getLongitude())));

		editor.apply();
	}

	public static void saveDistance(String distance, Context context) {
		Log.d(MyCashClientConstants.TAG, "distance -> " + distance);

		SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putString(MyCashClientConstants.DISTANCE, distance);

		editor.apply();
	}

	public static double getLatitude(Context context) {
		String latitude = getSharedPreferences(context).getString(MyCashClientConstants.LATITUDE, null);
		return (latitude != null) ? Double.parseDouble(latitude): -1;
	}

	public static double getLongitude(Context context) {
		String longitude = getSharedPreferences(context).getString(MyCashClientConstants.LONGITUDE, null);
		return (longitude != null) ? Double.parseDouble(longitude): -1;
	}

	public static String getImagesBaseUrl (Context context) {
        return getSharedPreferences(context).getString(MyCashClientConstants.IMAGES_BASE_URL, "");
	}

	public static String getDistance(Context context) {
		return getSharedPreferences(context).getString(MyCashClientConstants.DISTANCE, null);
	}

	public static String getPhoneNumber(Context context){
		return getSharedPreferences(context).getString(MyCashClientConstants.CCC, "")
			 + getSharedPreferences(context).getString(MyCashClientConstants.PHONE, "");
	}

	public static String getLoggedInUserName(Context context){
		return getSharedPreferences(context).getString(MyCashClientConstants.FILE_NAME, "") +
				" " + getSharedPreferences(context).getString(MyCashClientConstants.LAST_NAME, "");
	}

	public static String getUserName(Context context){
		return getSharedPreferences(context).getString(MyCashClientConstants.FILE_NAME, "");
	}

	public static String getUserLastName(Context context){
		return getSharedPreferences(context).getString(MyCashClientConstants.LAST_NAME, "");
	}

	public static String getUserEmail(Context context){
		return getSharedPreferences(context).getString(MyCashClientConstants.EMAIL, "");
	}

	/**
	 * This function removes the token after user sing
	 * outs from app
	 *
	 * @param context
	 */
	public static void removeToken (Context context) {
		Log.d(MyCashClientConstants.TAG, "remove token");
		SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.remove(MyCashClientConstants.TOKEN);
		editor.apply();
	}

	/**
	 *
	 * @param context
	 * @return
	 */
	public static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(MyCashClientConstants.FILE_NAME, MODE_PRIVATE);
	}

	/**
	 * This function returns the stored token for a
	 * particular user
	 *
	 * @param context
	 * @return
	 */
	public static String getToken(Context context) {
		return getSharedPreferences(context).getString(MyCashClientConstants.TOKEN, null);
	}

	@SuppressLint("RestrictedApi")
	public static void disableShiftMode(BottomNavigationView view) {
		BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
		try {
			Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
			shiftingMode.setAccessible(true);
			shiftingMode.setBoolean(menuView, false);
			shiftingMode.setAccessible(false);
			for (int i = 0; i < menuView.getChildCount(); i++) {
				BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
				//noinspection RestrictedApi
				//item.setShiftingMode(false);
				// set once again checked value, so view will be updated
				//noinspection RestrictedApi
				item.setChecked(item.getItemData().isChecked());
			}
		} catch (NoSuchFieldException e) {
			Log.e("BNVHelper", "Unable to get shift mode field", e);
		} catch (IllegalAccessException e) {
			Log.e("BNVHelper", "Unable to change value of shift mode", e);
		}
	}

	public static String generateQRContentStr (int amount, int bonus, int buyerId, int cash, boolean isBonusSwitched){
		int fromBonus = 0;
		if(isBonusSwitched){
			if(amount > bonus){
				fromBonus = bonus;
			}else{
				fromBonus = amount;
			}
		}
		return  "Pay [\"amount\": "+ amount +", " +
				"\"card_id\": 0," +
				"\"card\": 0," +
				"\"buyer_id\": "+ buyerId +"," +
				"\"fromBonus\": "+ fromBonus +"," +
				"\"cash\": "+ cash +"]";
	}

	/**
	 *
	 * @param webUrl
	 * @param context
	 */
	public static void openWebURL (String webUrl, Context context) {
		if (Patterns.WEB_URL.matcher(webUrl).matches()){
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(webUrl));
			context.startActivity(i);
		}
	}

	public static String encryptedString(String text) {
		Log.v("ENCRYPTED", "::text: " + text);
		String encodedText = Base64.encodeToString(text.getBytes(), android.util.Base64.NO_WRAP);//Base64
		String mixedText = "";
		for (int i = 0; i < encodedText.length(); i++) {
			mixedText += encodedText.charAt(i) + generateRandomChars(1);
		}

		Log.v("ENCRYPTED", "::encryptedString : " + mixedText);
		return mixedText;
	}

	public static String generateRandomChars(int length) {
		String candidateChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(candidateChars.charAt(random.nextInt(candidateChars
					.length())));
		}

		return sb.toString();
	}
}
