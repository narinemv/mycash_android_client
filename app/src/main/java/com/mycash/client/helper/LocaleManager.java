package com.mycash.client.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

public class LocaleManager {

    public static final  String LANGUAGE_ENGLISH   = "en";
    public static final  String LANGUAGE_RUSSIAN   = "ru";
    public static final  String LANGUAGE_ARMENIAN  = "hy";
    public static final  String LANGUAGE_ARMENIAN_COUNTRY  = "am";
    public static final  String LANGUAGE_ENGLISH_COUNTRY  = "us";
    private static final String LANGUAGE_KEY       = "language_key";

    public static Context setLocale(Context c) {
        return updateResources(c, getLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    public static String getLanguage(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString(LANGUAGE_KEY, LANGUAGE_ENGLISH);
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(Context c, String language) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        // use commit() instead of apply(), because sometimes we kill the application process immediately
        // which will prevent apply() to finish
        prefs.edit().putString(LANGUAGE_KEY, language).commit();
    }

    private static Context updateResources(Context context, String language) {
        Locale locale;

        Configuration config = new Configuration();

        if(language.equalsIgnoreCase(LANGUAGE_ENGLISH)) {

            locale = new Locale(LANGUAGE_ENGLISH);

            Locale.setDefault(locale);

            config.locale = locale;

        }else if(language.equalsIgnoreCase(LANGUAGE_ARMENIAN)){

            locale = new Locale(LANGUAGE_ARMENIAN);

            Locale.setDefault(locale);

            config.locale = locale;

        }else if(language.equalsIgnoreCase(LANGUAGE_RUSSIAN)){

            locale = new Locale(LANGUAGE_RUSSIAN);

            Locale.setDefault(locale);

            config.locale = locale;
        }

        context.getResources().updateConfiguration(config, null);

        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }

    public static String convertCodeCountryToLanguage(String code) {
        if (code.equalsIgnoreCase("am")){
            return LANGUAGE_ARMENIAN;
        }else if(code.equalsIgnoreCase("us")){
            return LANGUAGE_ENGLISH;
        }else if(code.equalsIgnoreCase("ru")){
            return LANGUAGE_RUSSIAN;
        }else {
            return LANGUAGE_RUSSIAN;
        }
    }

    public static String convertCodeLanguageToCountry(String code) {
        if (code.equalsIgnoreCase(LANGUAGE_ARMENIAN)){
            return LANGUAGE_ARMENIAN_COUNTRY;
        }else if(code.equalsIgnoreCase(LANGUAGE_ENGLISH)){
            return LANGUAGE_ENGLISH_COUNTRY;
        }else if(code.equalsIgnoreCase(LANGUAGE_RUSSIAN)){
            return LANGUAGE_RUSSIAN;
        }else{
            return LANGUAGE_RUSSIAN;
        }
    }

    public static boolean isCountryCodeHasSupportedLanguage (String code){
        if(code.equalsIgnoreCase(LocaleManager.LANGUAGE_RUSSIAN) ||
           code.equalsIgnoreCase(LocaleManager.LANGUAGE_ARMENIAN_COUNTRY) ||
           code.equalsIgnoreCase(LocaleManager.LANGUAGE_ENGLISH_COUNTRY)) {
            return true;
        }
        return false;
    }
}