package com.mycash.client.helper;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.mycash.client.ui.view.IPayInCashView;

/**
 * Created by Narine on 07,April,2018
 */
public class CreateBitMapTask extends AsyncTask<Void, Void, Bitmap> {

    String content;
    Bitmap bmp;
    IPayInCashView payInCashView;

    public CreateBitMapTask(String content, IPayInCashView payInCashView) {
        this.content = content;
        this.payInCashView = payInCashView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if( payInCashView != null) {
            payInCashView.showProgress();
        }
    }

    @Override protected Bitmap doInBackground(Void... params) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 300, 300);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return bmp;
    }

    @Override protected void onPostExecute(Bitmap bitmap) {
        if( payInCashView != null){
            payInCashView.hideProgress();
            payInCashView.passQRImage(bitmap);
        }
    }
}

