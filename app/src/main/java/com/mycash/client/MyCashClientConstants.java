package com.mycash.client;

/**
 * Created by Narine on 08,March,2018
 */
public class MyCashClientConstants {

    public static final String TAG = "MyCashClient";
    public static final String TOKEN_PREFIX = "Basic ";
    public static final String TOKEN = "token";
    public static final String FILE_NAME = "mycash_client_pref";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String CCC = "ccc";
    public static final String REGISTER_DATE = "register_date";
    public static final String QR_CONTENT = "qr_content";
    public static final String DISTANCE = "distance";
    public static final String SUB_CATEGORY_LIST = "sub_category_list";
    public static final String SUB_CATEGORY_NUM = "sub_category_num";
    public static final String SUB_CATEGORY_NAME = "sub_category_name";
    public static final String CATEGORY_IMAGE = "category_image";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_COLOR = "category_color";
    public static final String BRANCHES_LIST = "branches_list";
    public static final String SELECTED_BRANCH = "selected_branch";
    public static final String SELECTED_BRANCH_NUM = "selected_branch_num";
    public static final String IMAGES_BASE_URL = "images_base_url";
    public static final String IMAGE_URLS = "image_urls";
    public static final String STORE_ID = "store_id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String LOGO_API = "api/Buyer/StoreLogo/";
    public static final String WEB_SITE_API = "api/Buyer/StoreSite/";
    public static final String OPEN_MAP_FROM_CATEGORY = "open_map_from_category";
    public static final String BONUS = "bonus";

    public static long DELAY = 1000; //1 seconds after user stops typin

    public enum TRANSFER_ERROR_TYPE {NO_BONUS}
}