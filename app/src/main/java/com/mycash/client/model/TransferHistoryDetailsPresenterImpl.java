package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.PaymentHistoryDetails;
import com.mycash.client.model.entity.TransferHistoryDetails;
import com.mycash.client.presenter.IHistoryDetailsPresenter;
import com.mycash.client.presenter.ITransferHistoryDetailsPresenter;
import com.mycash.client.presenter.ITransferHistoryPresenter;
import com.mycash.client.ui.view.IHistoryDetailsView;
import com.mycash.client.ui.view.ITransferHistoryDetailsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 01,June,2018
 */
public class TransferHistoryDetailsPresenterImpl implements ITransferHistoryDetailsPresenter{

    private ITransferHistoryDetailsView transferDetailsView;
    private NetworkService service;

    public TransferHistoryDetailsPresenterImpl(ITransferHistoryDetailsView transferDetailsView, NetworkService service){
        this.transferDetailsView = transferDetailsView;
        this.service = service;
    }

    /**
     * Handling response callback directly in the form of lambdas
     *
     * @param transferDetails
     */
    private void handleResponse(TransferHistoryDetails transferDetails) {
        if( transferDetailsView != null){
            transferDetailsView.hideProgress();

            if( transferDetails != null) {
                transferDetailsView.showTransferHistoryDetails(transferDetails);
            }
        }
    }

    /**
     * Handling error callback directly in the form of lambdas
     *
     * @param error
     */
    private void handleError(Throwable  error) {
        if( transferDetailsView != null) {
            transferDetailsView.hideProgress();
            transferDetailsView.showTransferHistoryDetailsResponseError(service.getErrorMessage(error));
        }
    }

    @Override
    public void getTransferredHistoryDetails(String transferId) {
        if( transferDetailsView != null){
            transferDetailsView.showProgress();
        }
        service.getAPI().getTrasferHistoryDetails(transferId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleResponse(responses), error -> handleError(error));
    }

    @Override
    public void onDestroy() {
        transferDetailsView = null;
    }
}
