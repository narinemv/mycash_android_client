package com.mycash.client.model;

import android.util.Base64;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.LoginResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginInteractorImpl implements ILoginInteractor {

    private String token;
    private NetworkService service;
    private CompositeDisposable compositeDisposable;
    private OnLoginFinishedListener listener;

    @Override
    public void login(String username, String password, final OnLoginFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {

        this.token = Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        this.listener = listener;
        this.service = service;
        this.compositeDisposable = compositeDisposable;

        compositeDisposable.add(service.getAPI().cashierLogin(MyCashClientConstants.TOKEN_PREFIX + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleResponse(responses), error -> handleError(error)));
    }

    private void handleResponse (LoginResponse response) {
        if( response != null){
            response.setToken(token);

            listener.onSuccess(response);
        }
    }

    private void handleError (Throwable error) {
        listener.onResponseError(service.getErrorMessage(error));
    }
}