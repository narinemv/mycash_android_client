package com.mycash.client.model;

import android.util.Base64;
import android.util.Log;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.ChangePasswordRequest;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.User;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 24,July,2018
 */
public class ForgotPasswordInteractorImpl implements IForgotPasswordInteractor {

    private NetworkService service;
    private String email;
    private CompositeDisposable compositeDisposable;
    private OnSendCodeFinishedListener listener1;
    private OnChangePasswordFinishedListener listener2;

    @Override
    public void sendCode (String email, OnSendCodeFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.service = service;
        this.listener1 = listener;
        this.email = email;
        this.compositeDisposable = compositeDisposable;

        Request request = new Request();
        request.setValue(email);

        compositeDisposable.add(service.getAPI().sendCodeToEmail(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleSendCodeToEmailResponse(responses), error -> handleSendCodeToEmailError(error)));
    }

    @Override
    public void changePassword(String code, String email, String newPassword, OnChangePasswordFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.service = service;
        this.listener2 = listener;
        this.email = email;
        this.compositeDisposable = compositeDisposable;

        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setCode(code);
        request.setEmail(email);
        request.setNewPassword(newPassword);

        compositeDisposable.add(service.getAPI().changePassword(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleChangePasswordResponse(responses), error -> handleChangePasswordError(error)));

    }

    private void handleSendCodeToEmailResponse(String response) {
        if (response != null) {
            listener1.onSendCodeSuccess(response);
        }
    }

    private void handleSendCodeToEmailError(Throwable error) {
        if (error != null) {
            listener1.onSendCodeResponseError(service.getErrorMessage(error));
        }
    }

    private void handleChangePasswordResponse(String response) {
        if (response != null) {
            listener2.onChangePasswordSuccess();
        }
    }

    private void handleChangePasswordError(Throwable error) {
        if (error != null) {
            listener2.onChangePasswordResponseError(service.getErrorMessage(error));
        }
    }
}
