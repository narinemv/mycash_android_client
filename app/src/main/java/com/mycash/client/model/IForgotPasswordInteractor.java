package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;

import io.reactivex.disposables.CompositeDisposable;

public interface IForgotPasswordInteractor {

    interface OnSendCodeFinishedListener {

        void onEmailEmpty();

        void onEmailNonValid();

        void onSendCodeSuccess(String response);

        void onSendCodeResponseError(String errorMsg);
    }

    void sendCode(String email, OnSendCodeFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);

    interface OnChangePasswordFinishedListener {

        void onCodeEmpty();

        void onNewPasswordEmpty();

        void onConfirmPasswordEmpty();

        void onPswdNotConfirmed();

        void onFieldsEmpty();

        void onChangePasswordSuccess();

        void onChangePasswordResponseError(String errorMsg);
    }

    void changePassword(String code, String email, String newPassword, OnChangePasswordFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);

}
