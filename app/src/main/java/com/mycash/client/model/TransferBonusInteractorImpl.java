package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.TransferBonus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 02,July,2018
 */
public class TransferBonusInteractorImpl implements ITransferBonusInteractor {

    private NetworkService service;
    private ITransferBonusInteractor.OnTransferBonusFinishedListener transferListener;
    private ITransferBonusInteractor.OnGetUserFinishedListener getUserListener;
    private ITransferBonusInteractor.OnGetBranchFinishedListener getBranchListener;

    @Override
    public void transfer(TransferBonus transferBonusRequest, OnTransferBonusFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
           transferListener = listener;
           this.service = service;

           compositeDisposable.add(service.getAPI().transferBonus(transferBonusRequest)
                   .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(transferBonus -> handleTransferBonusResponse(transferBonus), error -> handleTransferBonusError(error)));
    }

    private void handleTransferBonusResponse (TransferBonus transferBonus) {
        transferListener.onTransferSuccess(transferBonus);
    }

    private void handleTransferBonusError (Throwable error) {
        transferListener.onTransferResponseError(service.getErrorMessage(error));
    }

    @Override
    public void getUserByPhone(Request phoneRequest, OnGetUserFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.getUserListener = listener;
        this.service = service;

        compositeDisposable.add(service.getAPI().CheckOtherBuyerState(phoneRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(phoneResponse -> handlePhoneResponse(phoneResponse), error -> handlePhoneError(error)));
    }

    private void handlePhoneResponse (CheckOtherBuyerStateResponse phoneResponse) {
        if( phoneResponse != null){
            getUserListener.onGetUserSuccess(phoneResponse);
        }
    }

    private void handlePhoneError (Throwable error) {
        getUserListener.onGetUserResponseError(service.getErrorMessage(error));
    }

    @Override
    public void getBranchByNumber(Request request, OnGetBranchFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.getBranchListener = listener;
        this.service = service;

        compositeDisposable.add(service.getAPI().getBranchResponse(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(branchResponse -> handleBranchResponse(branchResponse), error -> handleBranchError(error)));
    }

    private void handleBranchResponse (BuyerBonusResponse branchResponse) {
        if( branchResponse != null){
            getBranchListener.onGetBranchSuccess(branchResponse);
        }
    }

    private void handleBranchError (Throwable error) {
        getBranchListener.onGetBranchResponseError(service.getErrorMessage(error));
    }
}
