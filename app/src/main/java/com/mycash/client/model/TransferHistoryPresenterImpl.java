package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.presenter.IHistoryPresenter;
import com.mycash.client.presenter.ITransferHistoryPresenter;
import com.mycash.client.ui.view.IHistoryView;
import com.mycash.client.ui.view.ITransferHistoryView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 16,April,2018
 */
public class TransferHistoryPresenterImpl implements ITransferHistoryPresenter {

    private ITransferHistoryView transferHistoryView;
    private NetworkService service;
    private CompositeDisposable compositeDisposable;

    public TransferHistoryPresenterImpl(ITransferHistoryView transferHistoryView, NetworkService service){
       this.transferHistoryView = transferHistoryView;
       this.service = service;
    }

    @Override
    public void getTransferHistoryData() {

        if(transferHistoryView != null){
           transferHistoryView.showProgress();
        }
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(service.getAPI().getTrasferHistory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transferList -> handleResponse(transferList), error -> handleError(error)));
    }

    /**
     * Handling response callback directly in the form of lambdas
     *
     * @param transferList
     */
    private void handleResponse(List<TransferBonus> transferList) {
        if( transferHistoryView != null){
            transferHistoryView.hideProgress();

            if( transferList != null && transferList.size() > 0) {
                transferHistoryView.showTransferHistoryData(transferList);
            }else {
                transferHistoryView.showNoResult();
            }
        }
    }

    /**
     * Handling error callback directly in the form of lambdas
     *
     * @param error
     */
    private void handleError(Throwable  error) {
        if( transferHistoryView != null) {
            transferHistoryView.hideProgress();
            transferHistoryView.showTransferHistoryResponseError(service.getErrorMessage(error));
        }
    }

    @Override
    public void onDestroy() {
        transferHistoryView = null;
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
    }
}
