package com.mycash.client.model;

import android.util.Base64;
import android.util.Log;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;
import com.mycash.client.model.entity.ValidateRequest;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 11,July,2018
 */
// Needed to cleanup and refactor after final decision
public class RegistrationInteractorImpl implements IRegistrationInteractor {

    private NetworkService service;
    private String password;
    private String token;
    private CompositeDisposable compositeDisposable;
    private IRegistrationInteractor.OnRegistrationFinishedListener listener;
    private IRegistrationInteractor.OnRegValidationFinishedListener listener2;

    @Override
    public void register(User user, String password, OnRegistrationFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.token = Base64.encodeToString((user.getCCC() + user.getPhone() + ":" + password).getBytes(), Base64.NO_WRAP);
        this.service = service;
        this.listener = listener;
        this.password = password;
        this.compositeDisposable = compositeDisposable;

//        compositeDisposable.add(service.getAPI().registration(user)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(responses -> handleResponse(responses), error -> handleError(error)));


        compositeDisposable.add(service.getAPI().registration(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleResponse(responses), error -> handleError(error)));

//        compositeDisposable.add(service.getAPI().registration(user)
//                .doOnNext(responses -> handleResponse(responses))
//                .doOnError(error -> handleError(error))
//                .flatMap(responses -> service.getAPI().cashierLogin(MyCashClientConstants.TOKEN_PREFIX + token))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(loginResponse -> handleLoginResponse(loginResponse), error -> handleLoginResponseError(error)));
    }

    @Override
    public void registrationValidation(String code, String phoneNumber, OnRegValidationFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable) {
        this.service = service;
        this.listener2 = listener;
        this.compositeDisposable = compositeDisposable;

        ValidateRequest request = new ValidateRequest();
        request.setCode(code);
        request.setPhone(phoneNumber);

//        compositeDisposable.add(service.getAPI().validateBuyer(request)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(responses -> handleRegValidateResponse(responses), error -> handleRegValidationError(error)));

//        compositeDisposable.add(service.getAPI().validateBuyer(request)
//                .doOnNext(responses -> handleRegValidateResponse(responses))
//                .doOnError(error -> handleRegValidationError(error))
//                .flatMap(responses -> service.getAPI().cashierLogin(MyCashClientConstants.TOKEN_PREFIX + token))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(loginResponse -> handleLoginResponse(loginResponse), error -> handleLoginResponseError(error)));
    }

    private void handleResponse(LoginResponse response) {
        if (response != null) {

        compositeDisposable.add(service.getAPI().cashierLogin(MyCashClientConstants.TOKEN_PREFIX + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginResponse -> handleLoginResponse(loginResponse), error -> handleLoginResponseError(error)));
        }
    }

    private void handleRegValidateResponse(String response) {
        //listener2.onValidationSuccess();
    }

    private void handleRegValidationError(Throwable error) {
        //listener2.onValidationResponseError(service.getErrorMessage(error));
    }

    private void handleError(Throwable error) {
        listener.onRegistrationResponseError(service.getErrorMessage(error));
    }

    private void handleLoginResponse(LoginResponse response) {
        if(response != null){
           response.setToken(token);
           listener.onLoginAfterRegSuccess(response);
        }
    }

    private void handleLoginResponseError(Throwable error) {
        listener.onLoginAfterRegFail(service.getErrorMessage(error));
    }
}
