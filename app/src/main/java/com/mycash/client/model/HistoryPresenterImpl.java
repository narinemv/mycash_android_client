package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.presenter.IHistoryPresenter;
import com.mycash.client.ui.view.IHistoryView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 16,April,2018
 */
public class HistoryPresenterImpl implements IHistoryPresenter {

    private static final String TAG = HistoryPresenterImpl.class.getSimpleName();
    private CompositeDisposable compositeDisposable;
    private IHistoryView paymentHistoryView;
    private NetworkService service;

    public HistoryPresenterImpl(IHistoryView paymentHistoryView, NetworkService service){
       this.paymentHistoryView = paymentHistoryView;
       this.compositeDisposable = new CompositeDisposable();
       this.service = service;
    }

    @Override
    public void getPaymentsHistoryData() {

        if(paymentHistoryView != null){
           paymentHistoryView.showProgress();
        }
        compositeDisposable.add(service.getAPI().getPayments()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleResponse(responses), error -> handleError(error)));
    }

    /**
     * Handling response callback directly in the form of lambdas
     *
     * @param paymentData
     */
    private void handleResponse(List<PaymentHistory> paymentData) {
        if( paymentHistoryView != null){
            paymentHistoryView.hideProgress();

            if( paymentData != null && paymentData.size() > 0) {
                paymentHistoryView.showPaymentHistoryData(paymentData);
            }else {
                paymentHistoryView.showNoResult();
            }
        }
    }

    /**
     * Handling error callback directly in the form of lambdas
     *
     * @param error
     */
    private void handleError(Throwable  error) {
        if( paymentHistoryView != null) {
            paymentHistoryView.hideProgress();
            paymentHistoryView.showPaymentHistoryResponseError(service.getErrorMessage(error));
        }
    }

    @Override
    public void onDestroy() {
        paymentHistoryView = null;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
