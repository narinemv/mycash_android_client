package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Narine on 03,July,2018
 */
public class TransferBonus implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("from_buyer_id")
    @Expose
    private Integer fromBuyerId;
    @SerializedName("to_buyer_id")
    @Expose
    private Integer toBuyerId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("from_buyer_phone")
    @Expose
    private String fromBuyerPhone;
    @SerializedName("to_buyer_phone")
    @Expose
    private String toBuyerPhone;
    @SerializedName("store_id")
    @Expose
    private Integer storeId;
    @SerializedName("user_name")
    @Expose
    private String userName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromBuyerId() {
        return fromBuyerId;
    }

    public void setFromBuyerId(Integer fromBuyerId) {
        this.fromBuyerId = fromBuyerId;
    }

    public Integer getToBuyerId() {
        return toBuyerId;
    }

    public void setToBuyerId(Integer toBuyerId) {
        this.toBuyerId = toBuyerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromBuyerPhone() {
        return fromBuyerPhone;
    }

    public void setFromBuyerPhone(String fromBuyerPhone) {
        this.fromBuyerPhone = fromBuyerPhone;
    }

    public String getToBuyerPhone() {
        return toBuyerPhone;
    }

    public void setToBuyerPhone(String toBuyerPhone) {
        this.toBuyerPhone = toBuyerPhone;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
