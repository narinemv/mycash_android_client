package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.PaymentHistoryDetails;
import com.mycash.client.presenter.IHistoryDetailsPresenter;
import com.mycash.client.ui.view.IHistoryDetailsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 01,June,2018
 */
public class HistoryDetailsPresenterImpl implements IHistoryDetailsPresenter{

    private static final String TAG = HistoryDetailsPresenterImpl.class.getSimpleName();
    private IHistoryDetailsView historyDetailsView;
    private CompositeDisposable compositeDisposable;
    private NetworkService service;

    public HistoryDetailsPresenterImpl(IHistoryDetailsView historyDetailsView, NetworkService service){
        this.historyDetailsView = historyDetailsView;
        this.compositeDisposable = new CompositeDisposable();
        this.service = service;
    }

    /**
     * Handling response callback directly in the form of lambdas
     *
     * @param paymentDetails
     */
    private void handleResponse(PaymentHistoryDetails paymentDetails) {
        if( historyDetailsView != null){
            historyDetailsView.hideProgress();

            if( paymentDetails != null) {
                historyDetailsView.showPaymentHistoryDetails(paymentDetails);
            }
        }
    }

    /**
     * Handling error callback directly in the form of lambdas
     *
     * @param error
     */
    private void handleError(Throwable  error) {
        if( historyDetailsView != null) {
            historyDetailsView.hideProgress();
            historyDetailsView.showPaymentHistoryDetailsResponseError(service.getErrorMessage(error));
        }
    }

    @Override
    public void getPaymentHistoryDetails(String paymentId) {
        if( historyDetailsView != null){
            historyDetailsView.showProgress();
        }
        compositeDisposable.add(service.getAPI().getPaymentDetails(paymentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responses -> handleResponse(responses), error -> handleError(error)));
    }

    @Override
    public void onDestroy() {
        historyDetailsView = null;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
