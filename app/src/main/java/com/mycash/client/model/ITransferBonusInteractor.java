package com.mycash.client.model;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.TransferBonus;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Narine on 02,July,2018
 */
public interface ITransferBonusInteractor {

    interface OnTransferBonusFinishedListener {

        void onBranchNumberError();

        void onBonusError();

        void onBonusError(MyCashClientConstants.TRANSFER_ERROR_TYPE error);

        void onPhoneError();

        void onTransferSuccess(TransferBonus transferBonus);

        void onTransferResponseError(String errorMsg);
    }

    interface OnGetUserFinishedListener {

        void onGetUserSuccess(CheckOtherBuyerStateResponse response);

        void onGetUserResponseError(String errorMsg);
    }

    interface OnGetBranchFinishedListener {

        void onGetBranchSuccess(BuyerBonusResponse branchResponse);

        void onGetBranchResponseError(String errorMsg);
    }

    void transfer(TransferBonus transferBonusRequest, ITransferBonusInteractor.OnTransferBonusFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);

    void getUserByPhone(Request phoneRequest, ITransferBonusInteractor.OnGetUserFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);

    void getBranchByNumber(Request request, ITransferBonusInteractor.OnGetBranchFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);
}
