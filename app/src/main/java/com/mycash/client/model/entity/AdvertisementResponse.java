package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Narine on 12,July,2018
 */
public class AdvertisementResponse {

    @SerializedName("advertisement")
    @Expose
    private Advertisement advertisement;
    @SerializedName("store")
    @Expose
    private BranchResponse store;

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    public BranchResponse getStore() {
        return store;
    }

    public void setStore(BranchResponse store) {
        this.store = store;
    }

    public class Advertisement {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("store_id")
        @Expose
        private Integer storeId;
        @SerializedName("img_url")
        @Expose
        private String imgUrl;
        @SerializedName("txt")
        @Expose
        private String txt;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTxt() {
            return txt;
        }

        public void setTxt(String txt) {
            this.txt = txt;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }
    }
}