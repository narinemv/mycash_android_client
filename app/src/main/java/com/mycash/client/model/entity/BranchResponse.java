package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Narine on 25,May,2018
 */
public class BranchResponse implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("cashback")
    @Expose
    private Integer cashback;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("work_start")
    @Expose
    private String workStart;
    @SerializedName("work_end")
    @Expose
    private String workEnd;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("cashiers_count")
    @Expose
    private Integer cashiersCount;
    @SerializedName("e_phone")
    @Expose
    private String ePhone;
    @SerializedName("about_us")
    @Expose
    private String aboutUs;
    @SerializedName("bonus_1")
    @Expose
    private Integer bonus1;
    @SerializedName("bonus_2")
    @Expose
    private Integer bonus2;
    @SerializedName("RUB_account")
    @Expose
    private String rUBAccount;
    @SerializedName("USD_account")
    @Expose
    private String uSDAccount;
    @SerializedName("AMD_account")
    @Expose
    private String aMDAccount;
    @SerializedName("GEL_account")
    @Expose
    private String gELAccount;
    @SerializedName("AMD_name")
    @Expose
    private String aMDName;
    @SerializedName("USD_name")
    @Expose
    private String uSDName;
    @SerializedName("RUB_name")
    @Expose
    private String rUBName;
    @SerializedName("GEL_name")
    @Expose
    private String gELName;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("suspended")
    @Expose
    private Boolean suspended;
    @SerializedName("Geolocation")
    @Expose
    private Object geolocation;
    @SerializedName("CCC")
    @Expose
    private Object cCC;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public Integer getCashback() {
        return cashback;
    }

    public void setCashback(Integer cashback) {
        this.cashback = cashback;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkStart() {
        return workStart;
    }

    public void setWorkStart(String workStart) {
        this.workStart = workStart;
    }

    public String getWorkEnd() {
        return workEnd;
    }

    public void setWorkEnd(String workEnd) {
        this.workEnd = workEnd;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCashiersCount() {
        return cashiersCount;
    }

    public void setCashiersCount(Integer cashiersCount) {
        this.cashiersCount = cashiersCount;
    }

    public String getEPhone() {
        return ePhone;
    }

    public void setEPhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public Integer getBonus1() {
        return bonus1;
    }

    public void setBonus1(Integer bonus1) {
        this.bonus1 = bonus1;
    }

    public Integer getBonus2() {
        return bonus2;
    }

    public void setBonus2(Integer bonus2) {
        this.bonus2 = bonus2;
    }

    public String getRUBAccount() {
        return rUBAccount;
    }

    public void setRUBAccount(String rUBAccount) {
        this.rUBAccount = rUBAccount;
    }

    public String getUSDAccount() {
        return uSDAccount;
    }

    public void setUSDAccount(String uSDAccount) {
        this.uSDAccount = uSDAccount;
    }

    public String getAMDAccount() {
        return aMDAccount;
    }

    public void setAMDAccount(String aMDAccount) {
        this.aMDAccount = aMDAccount;
    }

    public String getGELAccount() {
        return gELAccount;
    }

    public void setGELAccount(String gELAccount) {
        this.gELAccount = gELAccount;
    }

    public String getAMDName() {
        return aMDName;
    }

    public void setAMDName(String aMDName) {
        this.aMDName = aMDName;
    }

    public String getUSDName() {
        return uSDName;
    }

    public void setUSDName(String uSDName) {
        this.uSDName = uSDName;
    }

    public String getRUBName() {
        return rUBName;
    }

    public void setRUBName(String rUBName) {
        this.rUBName = rUBName;
    }

    public String getGELName() {
        return gELName;
    }

    public void setGELName(String gELName) {
        this.gELName = gELName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Object getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Object geolocation) {
        this.geolocation = geolocation;
    }

    public Object getCCC() {
        return cCC;
    }

    public void setCCC(Object cCC) {
        this.cCC = cCC;
    }
}
