package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.helper.CreateBitMapTask;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.presenter.IPayInCashPresenter;
import com.mycash.client.ui.view.IPayInCashView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Narine on 04,April,2018
 */
public class PayInCashInteractorImpl implements IPayInCashPresenter {

    private IPayInCashView payInCashViewListener;
    private NetworkService service;
    private CompositeDisposable compositeDisposable;

    public PayInCashInteractorImpl(IPayInCashView view, NetworkService service){
        this.payInCashViewListener = view;
        this.service = service;
    }

    @Override
    public void loadRxData(String branchId) {

        Request request = new Request();
        request.setValue(branchId);

        if(payInCashViewListener != null){
           payInCashViewListener.showProgress();

        }
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(service.getAPI().getBranchResponse(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(branchResponse -> handleResponse(branchResponse), error -> handleError(error)));
    }

    private void handleResponse(BuyerBonusResponse branchResponse) {
        if( payInCashViewListener != null) {
            payInCashViewListener.hideProgress();
            payInCashViewListener.showBuyerBonusResponseResults(branchResponse);
        }
    }

    private void handleError(Throwable  error) {
        if( payInCashViewListener != null) {
            payInCashViewListener.hideProgress();

            payInCashViewListener.showBuyerBonusResponseFailure(service.getErrorMessage(error));
        }
    }

    @Override
    public void rxUnSubscribe() {
         if( compositeDisposable != null) {
             compositeDisposable.dispose();
         }
    }

    @Override
    public void onDestroy() {
        payInCashViewListener = null;
        if(compositeDisposable != null)
           compositeDisposable.dispose();
    }

    @Override
    public void calculateCash(int amount, int bonus, boolean isBonusSwitched) {

        int cash = 0;
        if( isBonusSwitched){
            if( bonus == 0){
                cash = amount;
            }else if(amount > bonus){
                cash = amount - bonus;
            }else {
                cash = 0;
            }
        }else {
            cash = amount;
        }

        if( payInCashViewListener != null) {
            payInCashViewListener.showCash(String.valueOf(cash));
        }
    }

    @Override
    public void generateQRImage(String qrContent) {
        new CreateBitMapTask(qrContent, payInCashViewListener).execute();
    }
}
