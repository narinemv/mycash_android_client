package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Narine on 16,April,2018
 */
public class PaymentHistory implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("cash")
    @Expose
    private Integer cash;

    @SerializedName("cashier_id")
    @Expose
    private Integer cashierId;

    @SerializedName("buyer_id")
    @Expose
    private Integer buyerId;

    @SerializedName("store_id")
    @Expose
    private Integer storeId;

    @SerializedName("bonus")
    @Expose
    private Integer bonus;

    @SerializedName("card")
    @Expose
    private Integer card;

    @SerializedName("sale")
    @Expose
    private Integer sale;

    @SerializedName("cash_back")
    @Expose
    private Integer cashBack;

    @SerializedName("bonus_club")
    @Expose
    private Integer bonusClub;

    @SerializedName("remainder")
    @Expose
    private Integer remainder;

    @SerializedName("commission")
    @Expose
    private Integer commission;

    @SerializedName("valid")
    @Expose
    private Boolean valid;

    @SerializedName("from_bonus")
    @Expose
    private Integer fromBonus;

    @SerializedName("bonus_charging")
    @Expose
    private Boolean bonusCharging;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public Integer getCashierId() {
        return cashierId;
    }

    public void setCashierId(Integer cashierId) {
        this.cashierId = cashierId;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public Integer getCard() {
        return card;
    }

    public void setCard(Integer card) {
        this.card = card;
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }

    public Integer getCashBack() {
        return cashBack;
    }

    public void setCashBack(Integer cashBack) {
        this.cashBack = cashBack;
    }

    public Integer getBonusClub() {
        return bonusClub;
    }

    public void setBonusClub(Integer bonusClub) {
        this.bonusClub = bonusClub;
    }

    public Integer getRemainder() {
        return remainder;
    }

    public void setRemainder(Integer remainder) {
        this.remainder = remainder;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Integer getFromBonus() {
        return fromBonus;
    }

    public void setFromBonus(Integer fromBonus) {
        this.fromBonus = fromBonus;
    }

    public Boolean getBonusCharging() {
        return bonusCharging;
    }

    public void setBonusCharging(Boolean bonusCharging) {
        this.bonusCharging = bonusCharging;
    }
}
