package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Narine on 04,July,2018
 */
public class CheckOtherBuyerStateResponse {

    @SerializedName("buyer_id")
    @Expose
    private Integer buyerId;
    @SerializedName("buyer_first_name")
    @Expose
    private String buyerFirstName;
    @SerializedName("buyer_last_name")
    @Expose
    private String buyerLastName;

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerFirstName() {
        return buyerFirstName;
    }

    public void setBuyerFirstName(String buyerFirstName) {
        this.buyerFirstName = buyerFirstName;
    }

    public String getBuyerLastName() {
        return buyerLastName;
    }

    public void setBuyerLastName(String buyerLastName) {
        this.buyerLastName = buyerLastName;
    }
}
