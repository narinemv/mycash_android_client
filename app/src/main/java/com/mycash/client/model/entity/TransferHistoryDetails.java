package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Narine on 10,July,2018
 */
public class TransferHistoryDetails {

    @SerializedName("transfer")
    @Expose
    private Transfer transfer;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("dealWithBuyer")
    @Expose
    private DealWithBuyer dealWithBuyer;

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public DealWithBuyer getDealWithBuyer() {
        return dealWithBuyer;
    }

    public void setDealWithBuyer(DealWithBuyer dealWithBuyer) {
        this.dealWithBuyer = dealWithBuyer;
    }

    public class DealWithBuyer {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("valid")
        @Expose
        private Boolean valid;
        @SerializedName("sms_code")
        @Expose
        private String smsCode;
        @SerializedName("reg_date")
        @Expose
        private String regDate;
        @SerializedName("CCC")
        @Expose
        private String cCC;
        @SerializedName("stripe_id")
        @Expose
        private Object stripeId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Boolean getValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }

        public String getSmsCode() {
            return smsCode;
        }

        public void setSmsCode(String smsCode) {
            this.smsCode = smsCode;
        }

        public String getRegDate() {
            return regDate;
        }

        public void setRegDate(String regDate) {
            this.regDate = regDate;
        }

        public String getCCC() {
            return cCC;
        }

        public void setCCC(String cCC) {
            this.cCC = cCC;
        }

        public Object getStripeId() {
            return stripeId;
        }

        public void setStripeId(Object stripeId) {
            this.stripeId = stripeId;
        }
    }

    public class Transfer {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("from_buyer_id")
        @Expose
        private Integer fromBuyerId;
        @SerializedName("to_buyer_id")
        @Expose
        private Integer toBuyerId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("from_buyer_phone")
        @Expose
        private String fromBuyerPhone;
        @SerializedName("to_buyer_phone")
        @Expose
        private String toBuyerPhone;
        @SerializedName("store_id")
        @Expose
        private Integer storeId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getFromBuyerId() {
            return fromBuyerId;
        }

        public void setFromBuyerId(Integer fromBuyerId) {
            this.fromBuyerId = fromBuyerId;
        }

        public Integer getToBuyerId() {
            return toBuyerId;
        }

        public void setToBuyerId(Integer toBuyerId) {
            this.toBuyerId = toBuyerId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getFromBuyerPhone() {
            return fromBuyerPhone;
        }

        public void setFromBuyerPhone(String fromBuyerPhone) {
            this.fromBuyerPhone = fromBuyerPhone;
        }

        public String getToBuyerPhone() {
            return toBuyerPhone;
        }

        public void setToBuyerPhone(String toBuyerPhone) {
            this.toBuyerPhone = toBuyerPhone;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }
    }
}
