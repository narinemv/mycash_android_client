package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Narine on 24,July,2018
 */
public class ChangePasswordRequest {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("newPassword")
    @Expose
    private String newPassword;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
