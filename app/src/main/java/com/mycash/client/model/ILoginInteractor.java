package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.LoginResponse;

import io.reactivex.disposables.CompositeDisposable;

public interface ILoginInteractor {

    interface OnLoginFinishedListener {
        void onPhoneError();

        void onPasswordError();

        void onFieldsEmpty();

        void onSuccess(LoginResponse loginResponse);

        void onResponseError(String errorMsg);
    }

    void login(String username, String password, OnLoginFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);
}