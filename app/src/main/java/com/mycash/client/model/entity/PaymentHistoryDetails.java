package com.mycash.client.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Narine on 16,April,2018
 */
public class PaymentHistoryDetails {

    @SerializedName("payment")
    @Expose
    private Payment payment;

    @SerializedName("store")
    @Expose
    private Store store;

    @SerializedName("total_bonus")
    @Expose
    private Integer totalBonus;

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Integer getTotalBonus() {
        return totalBonus;
    }

    public void setTotalBonus(Integer totalBonus) {
        this.totalBonus = totalBonus;
    }


    public class Payment {

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("date")
        @Expose
        private String date;

        @SerializedName("cash")
        @Expose
        private Integer cash;

        @SerializedName("cashier_id")
        @Expose
        private Integer cashierId;

        @SerializedName("buyer_id")
        @Expose
        private Integer buyerId;

        @SerializedName("store_id")
        @Expose
        private Integer storeId;

        @SerializedName("bonus")
        @Expose
        private Integer bonus;

        @SerializedName("card")
        @Expose
        private Integer card;

        @SerializedName("sale")
        @Expose
        private Integer sale;

        @SerializedName("cash_back")
        @Expose
        private Integer cashBack;

        @SerializedName("bonus_club")
        @Expose
        private Integer bonusClub;

        @SerializedName("remainder")
        @Expose
        private Integer remainder;

        @SerializedName("commission")
        @Expose
        private Integer commission;

        @SerializedName("valid")
        @Expose
        private Boolean valid;

        @SerializedName("from_bonus")
        @Expose
        private Integer fromBonus;

        @SerializedName("bonus_charging")
        @Expose
        private Boolean bonusCharging;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getCash() {
            return cash;
        }

        public void setCash(Integer cash) {
            this.cash = cash;
        }

        public Integer getCashierId() {
            return cashierId;
        }

        public void setCashierId(Integer cashierId) {
            this.cashierId = cashierId;
        }

        public Integer getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(Integer buyerId) {
            this.buyerId = buyerId;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public Integer getBonus() {
            return bonus;
        }

        public void setBonus(Integer bonus) {
            this.bonus = bonus;
        }

        public Integer getCard() {
            return card;
        }

        public void setCard(Integer card) {
            this.card = card;
        }

        public Integer getSale() {
            return sale;
        }

        public void setSale(Integer sale) {
            this.sale = sale;
        }

        public Integer getCashBack() {
            return cashBack;
        }

        public void setCashBack(Integer cashBack) {
            this.cashBack = cashBack;
        }

        public Integer getBonusClub() {
            return bonusClub;
        }

        public void setBonusClub(Integer bonusClub) {
            this.bonusClub = bonusClub;
        }

        public Integer getRemainder() {
            return remainder;
        }

        public void setRemainder(Integer remainder) {
            this.remainder = remainder;
        }

        public Integer getCommission() {
            return commission;
        }

        public void setCommission(Integer commission) {
            this.commission = commission;
        }

        public Boolean getValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }

        public Integer getFromBonus() {
            return fromBonus;
        }

        public void setFromBonus(Integer fromBonus) {
            this.fromBonus = fromBonus;
        }

        public Boolean getBonusCharging() {
            return bonusCharging;
        }

        public void setBonusCharging(Boolean bonusCharging) {
            this.bonusCharging = bonusCharging;
        }
    }

    /*public class Store {

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("address")
        @Expose
        private String address;

        @SerializedName("categories")
        @Expose
        private String categories;

        @SerializedName("cashback")
        @Expose
        private Integer cashback;

        @SerializedName("discount")
        @Expose
        private Integer discount;

        @SerializedName("latitude")
        @Expose
        private Double latitude;

        @SerializedName("longitude")
        @Expose
        private Double longitude;

        @SerializedName("phone")
        @Expose
        private String phone;

        @SerializedName("work_start")
        @Expose
        private String workStart;

        @SerializedName("work_end")
        @Expose
        private String workEnd;

        @SerializedName("user_id")
        @Expose
        private Integer userId;

        @SerializedName("cashiers_count")
        @Expose
        private Integer cashiersCount;

        @SerializedName("e_phone")
        @Expose
        private String ePhone;

        @SerializedName("about_us")
        @Expose
        private String aboutUs;

        @SerializedName("bonus_1")
        @Expose
        private Integer bonus1;

        @SerializedName("bonus_2")
        @Expose
        private Integer bonus2;

        @SerializedName("RUB_account")
        @Expose
        private Object rUBAccount;

        @SerializedName("USD_account")
        @Expose
        private Object uSDAccount;

        @SerializedName("AMD_account")
        @Expose
        private Object aMDAccount;

        @SerializedName("GEL_account")
        @Expose
        private Object gELAccount;

        @SerializedName("AMD_name")
        @Expose
        private Object aMDName;

        @SerializedName("USD_name")
        @Expose
        private Object uSDName;

        @SerializedName("RUB_name")
        @Expose
        private Object rUBName;

        @SerializedName("GEL_name")
        @Expose
        private Object gELName;

        @SerializedName("video_url")
        @Expose
        private Object videoUrl;

        @SerializedName("image_url")
        @Expose
        private Object imageUrl;

        @SerializedName("suspended")
        @Expose
        private Boolean suspended;

        @SerializedName("Geolocation")
        @Expose
        private Object geolocation;

        @SerializedName("CCC")
        @Expose
        private Object cCC;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCategories() {
            return categories;
        }

        public void setCategories(String categories) {
            this.categories = categories;
        }

        public Integer getCashback() {
            return cashback;
        }

        public void setCashback(Integer cashback) {
            this.cashback = cashback;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getWorkStart() {
            return workStart;
        }

        public void setWorkStart(String workStart) {
            this.workStart = workStart;
        }

        public String getWorkEnd() {
            return workEnd;
        }

        public void setWorkEnd(String workEnd) {
            this.workEnd = workEnd;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCashiersCount() {
            return cashiersCount;
        }

        public void setCashiersCount(Integer cashiersCount) {
            this.cashiersCount = cashiersCount;
        }

        public String getEPhone() {
            return ePhone;
        }

        public void setEPhone(String ePhone) {
            this.ePhone = ePhone;
        }

        public String getAboutUs() {
            return aboutUs;
        }

        public void setAboutUs(String aboutUs) {
            this.aboutUs = aboutUs;
        }

        public Integer getBonus1() {
            return bonus1;
        }

        public void setBonus1(Integer bonus1) {
            this.bonus1 = bonus1;
        }

        public Integer getBonus2() {
            return bonus2;
        }

        public void setBonus2(Integer bonus2) {
            this.bonus2 = bonus2;
        }

        public Object getRUBAccount() {
            return rUBAccount;
        }

        public void setRUBAccount(Object rUBAccount) {
            this.rUBAccount = rUBAccount;
        }

        public Object getUSDAccount() {
            return uSDAccount;
        }

        public void setUSDAccount(Object uSDAccount) {
            this.uSDAccount = uSDAccount;
        }

        public Object getAMDAccount() {
            return aMDAccount;
        }

        public void setAMDAccount(Object aMDAccount) {
            this.aMDAccount = aMDAccount;
        }

        public Object getGELAccount() {
            return gELAccount;
        }

        public void setGELAccount(Object gELAccount) {
            this.gELAccount = gELAccount;
        }

        public Object getAMDName() {
            return aMDName;
        }

        public void setAMDName(Object aMDName) {
            this.aMDName = aMDName;
        }

        public Object getUSDName() {
            return uSDName;
        }

        public void setUSDName(Object uSDName) {
            this.uSDName = uSDName;
        }

        public Object getRUBName() {
            return rUBName;
        }

        public void setRUBName(Object rUBName) {
            this.rUBName = rUBName;
        }

        public Object getGELName() {
            return gELName;
        }

        public void setGELName(Object gELName) {
            this.gELName = gELName;
        }

        public Object getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(Object videoUrl) {
            this.videoUrl = videoUrl;
        }

        public Object getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(Object imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Boolean getSuspended() {
            return suspended;
        }

        public void setSuspended(Boolean suspended) {
            this.suspended = suspended;
        }

        public Object getGeolocation() {
            return geolocation;
        }

        public void setGeolocation(Object geolocation) {
            this.geolocation = geolocation;
        }

        public Object getCCC() {
            return cCC;
        }

        public void setCCC(Object cCC) {
            this.cCC = cCC;
        }
    }*/
}
