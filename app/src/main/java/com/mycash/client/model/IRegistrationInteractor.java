package com.mycash.client.model;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;

import io.reactivex.disposables.CompositeDisposable;

public interface IRegistrationInteractor {

    interface OnRegistrationFinishedListener {
        void onFirstNameEmpty();

        void onLastNameEmpty();

        void onPasswordEmpty();

        void onConfirmPasswordEmpty();

        void onPhoneEmpty();

        void onEmailEmpty();

        void onFieldsEmpty();

        void onEmailNonValid();

        void onPswdNotConfirmed();

        void onRegistrationSuccess(LoginResponse response);

        void onRegistrationResponseError(String errorMsg);

        void onLoginAfterRegSuccess(LoginResponse response);

        void onLoginAfterRegFail(String errorMsg);
    }
    void register(User registration, String password, OnRegistrationFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);

    interface OnRegValidationFinishedListener {

        void onCodeEmpty();

        void onValidationSuccess(LoginResponse response);

        void onValidationResponseError(String errorMsg);
    }
    void registrationValidation (String code, String phoneNumber, OnRegValidationFinishedListener listener, NetworkService service, CompositeDisposable compositeDisposable);
}
