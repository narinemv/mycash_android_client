package com.mycash.client.presenter;

/**
 * Created by Narine on 01,June,2018
 */
public interface IHistoryDetailsPresenter {

    void getPaymentHistoryDetails (String paymentId);

    void onDestroy();
}

