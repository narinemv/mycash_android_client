package com.mycash.client.presenter;

import android.text.TextUtils;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.ForgotPasswordInteractorImpl;
import com.mycash.client.model.IForgotPasswordInteractor;
import com.mycash.client.model.IRegistrationInteractor;
import com.mycash.client.model.RegistrationInteractorImpl;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;
import com.mycash.client.ui.view.IForgotPasswordView;
import com.mycash.client.ui.view.IRegisterView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Narine on 24,July,2018
 */
public class ForgotPasswordPresenterImpl implements IForgotPasswordPresenter, IForgotPasswordInteractor.OnSendCodeFinishedListener,
                                                                              IForgotPasswordInteractor.OnChangePasswordFinishedListener{

    private CompositeDisposable compositeDisposable;
    private IForgotPasswordInteractor listener;
    private NetworkService service;
    private String email;
    private IForgotPasswordView forgotPasswordView;

    public ForgotPasswordPresenterImpl(IForgotPasswordView forgotPasswordView, NetworkService service, ForgotPasswordInteractorImpl listener) {
        this.forgotPasswordView = forgotPasswordView;
        this.listener = listener;
        this.service = service;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void validateEmail(String email) {

        if (forgotPasswordView != null){
            forgotPasswordView.showProgress();
        }

        if (TextUtils.isEmpty(email)) {
            onEmailEmpty();
            return;
        }

        if (!isValidEmail(email)){
            onEmailNonValid();
            return;
        }

        this.email = email;
        listener.sendCode(email, this, service, compositeDisposable);
    }

    @Override
    public void validateChangePasswordFields(String code, String newPassword, String confirmPassword) {
        if (forgotPasswordView != null){
            forgotPasswordView.showProgress();
        }

        if (TextUtils.isEmpty(code) && TextUtils.isEmpty(newPassword) && TextUtils.isEmpty(confirmPassword)) {
            onFieldsEmpty();
            return;
        }

        if (TextUtils.isEmpty(code)) {
            onCodeEmpty();
            return;
        }

        if (TextUtils.isEmpty(newPassword)) {
            onNewPasswordEmpty();
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            onConfirmPasswordEmpty();
            return;
        }

        if (!isValidPassword(newPassword, confirmPassword)){
            onPswdNotConfirmed();
            return;
        }

        listener.changePassword(code, email, newPassword, this, service, compositeDisposable);
    }

    @Override
    public void onEmailEmpty() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setEmailEmpty();
        }
    }

    @Override
    public void onEmailNonValid() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setEmailNonValid();
        }
    }

    @Override
    public void onSendCodeSuccess(String response) {
         if (forgotPasswordView != null){
             forgotPasswordView.hideProgress();

             forgotPasswordView.showChangePassFields(response);
         }
    }

    @Override
    public void onSendCodeResponseError(String errorMsg) {
        if (forgotPasswordView != null){
            forgotPasswordView.hideProgress();

            forgotPasswordView.showSendCodeToEmailError(errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        forgotPasswordView = null;
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
    }

    public static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        email = email.replaceAll(" ", "");
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    private boolean isValidPassword (String password, String confirmedPassword){
        return password.equals(confirmedPassword);
    }

    @Override
    public void onCodeEmpty() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setCodeEmpty();
        }
    }

    @Override
    public void onNewPasswordEmpty() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setNewPasswordEmpty();
        }
    }

    @Override
    public void onConfirmPasswordEmpty() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setConfirmPasswordEmpty();
        }
    }

    @Override
    public void onPswdNotConfirmed() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setPswdNotConfirmed();
        }
    }

    @Override
    public void onFieldsEmpty() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.setFieldsEmpty();
        }
    }

    @Override
    public void onChangePasswordSuccess() {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.finishChangePassword();
        }
    }

    @Override
    public void onChangePasswordResponseError(String errorMsg) {
        if (forgotPasswordView != null) {
            forgotPasswordView.hideProgress();

            forgotPasswordView.showChangePasswordError(errorMsg);
        }
    }
}

