package com.mycash.client.presenter;

public interface IForgotPasswordPresenter {

    void validateEmail(String email);

    void validateChangePasswordFields (String code, String newPassword, String confirmPassword);

    void onDestroy();
}