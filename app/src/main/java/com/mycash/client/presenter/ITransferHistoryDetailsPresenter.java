package com.mycash.client.presenter;

/**
 * Created by Narine on 01,June,2018
 */
public interface ITransferHistoryDetailsPresenter {

    void getTransferredHistoryDetails(String transferId);

    void onDestroy();
}

