package com.mycash.client.presenter;

import android.text.TextUtils;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.IRegistrationInteractor;
import com.mycash.client.model.RegistrationInteractorImpl;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;
import com.mycash.client.ui.view.IRegisterView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Narine on 07,July,2018
 */
public class RegistrationPresenterImpl implements IRegistrationPresenter, IRegistrationInteractor.OnRegistrationFinishedListener,
                                                                           IRegistrationInteractor.OnRegValidationFinishedListener{

    private CompositeDisposable compositeDisposable;
    private IRegistrationInteractor listener;
    private NetworkService service;
    private IRegisterView regView;
    private String phone;

    public RegistrationPresenterImpl(IRegisterView regView, NetworkService service, RegistrationInteractorImpl listener) {
        this.regView = regView;
        this.listener = listener;
        this.service = service;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void validateRegistrationCredentials(String ccc,
                                                String firstName,
                                                String lastName,
                                                String email,
                                                String phone,
                                                String password,
                                                String confirmPassword) {

        if (regView != null) {
            regView.showProgress();
        }

        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && TextUtils.isEmpty(email)
                && TextUtils.isEmpty(phone) && TextUtils.isEmpty(password) && TextUtils.isEmpty(confirmPassword)){
            onFieldsEmpty();
            return;
        }

        if (TextUtils.isEmpty(firstName)) {
            onFirstNameEmpty();
            return;
        }

        if (TextUtils.isEmpty(lastName)){
            onLastNameEmpty();
            return;
        }

        if (TextUtils.isEmpty(phone)){
            onPhoneEmpty();
            return;
        }

        if (TextUtils.isEmpty(email)){
            onEmailEmpty();
            return;
        }

        if (TextUtils.isEmpty(password)){
            onPasswordEmpty();
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)){
            onConfirmPasswordEmpty();
            return;
        }

        if (!isValidEmail(email)){
            onEmailNonValid();
            return;
        }

        if (!isValidPassword(password, confirmPassword)){
            onPswdNotConfirmed();
            return;
        }

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhone(phone);
        user.setEmail(email);
        user.setCCC(ccc);
        user.setPassword(password);

        this.phone = ccc+phone;

        listener.register(user, password, this, service, compositeDisposable);
    }

    @Override
    public void validateCode(String code) {
        if(regView != null){
           regView.showProgress();
        }

        if (TextUtils.isEmpty(code)){
            onCodeEmpty();
            return;
        }

        listener.registrationValidation(code, phone, this, service, compositeDisposable);
    }

    @Override
    public void onFirstNameEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setFirstNameEmpty();
        }
    }

    @Override
    public void onLastNameEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setLastNameEmpty();
        }
    }

    @Override
    public void onPasswordEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setPasswordEmpty();
        }
    }

    @Override
    public void onConfirmPasswordEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setConfirmPasswordEmpty();
        }
    }

    @Override
    public void onPhoneEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setPhoneEmpty();
        }
    }

    @Override
    public void onEmailEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setEmailEmpty();
        }
    }

    @Override
    public void onFieldsEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setFieldsEmpty();
        }
    }

    @Override
    public void onEmailNonValid() {
        if (regView != null) {
            regView.hideProgress();

            regView.setEmailNonValid();
        }
    }

    @Override
    public void onPswdNotConfirmed() {
        if (regView != null) {
            regView.hideProgress();

            regView.setPswdNotConfirmed();
        }
    }

    @Override
    public void onRegistrationSuccess(LoginResponse response) {
        if (regView != null) {
            regView.hideProgress();

            regView.navigateToHome(response);
        }
    }

    @Override
    public void onRegistrationResponseError(String errorMsg) {
        if (regView != null) {
            regView.hideProgress();

            regView.setRegistrationResponseError(errorMsg);
        }
    }

    @Override
    public void onLoginAfterRegSuccess(LoginResponse response) {
        if (regView != null) {
            regView.hideProgress();

            regView.goToLoginPage(response);
        }
    }

    @Override
    public void onLoginAfterRegFail(String errorMsg) {
        if (regView != null) {
            regView.hideProgress();
            //didn't create setLoginAfterRegError() because of old implementation could be revert
            regView.setValidationResponseError(errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        regView = null;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    public static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        email = email.replaceAll(" ", "");
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    private boolean isValidPassword (String password, String confirmedPassword){
        return password.equals(confirmedPassword);
    }

    @Override
    public void onCodeEmpty() {
        if (regView != null) {
            regView.hideProgress();

            regView.setCodeEmpty();
        }
    }

    @Override
    public void onValidationSuccess(LoginResponse response) {
        if (regView != null) {
            regView.hideProgress();

            regView.goToLoginPage(response);
        }
    }

    @Override
    public void onValidationResponseError(String errorMsg) {
        if (regView != null) {
            regView.hideProgress();

            regView.setValidationResponseError(errorMsg);
        }
    }
}

