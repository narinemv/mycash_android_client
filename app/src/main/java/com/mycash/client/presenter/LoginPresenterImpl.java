package com.mycash.client.presenter;

import android.text.TextUtils;

import com.mycash.client.api.NetworkService;
import com.mycash.client.model.ILoginInteractor;
import com.mycash.client.model.LoginInteractorImpl;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.ui.view.ILoginView;

import io.reactivex.disposables.CompositeDisposable;

public class LoginPresenterImpl implements ILoginPresenter, ILoginInteractor.OnLoginFinishedListener {

    private ILoginView loginView;
    private ILoginInteractor listener;
    private NetworkService service;
    private CompositeDisposable compositeDisposable;

    public LoginPresenterImpl(ILoginView loginView, NetworkService service, LoginInteractorImpl listener) {
        this.loginView = loginView;
        this.listener = listener;
        this.service = service;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void validateCredentials(String ccc, String phone, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }

        if (TextUtils.isEmpty(phone) && TextUtils.isEmpty(password)){
            onFieldsEmpty();
            return;
        }

        if (TextUtils.isEmpty(phone)) {
            onPhoneError();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            onPasswordError();
            return;
        }

        listener.login(ccc + phone, password, this, service, compositeDisposable);
    }

    @Override
    public void onPhoneError() {
        if (loginView != null) {
            loginView.hideProgress();

            loginView.setUsernameError();
        }
    }

    @Override
    public void onPasswordError() {
        if (loginView != null) {
            loginView.hideProgress();

            loginView.setPasswordError();
        }
    }

    @Override
    public void onFieldsEmpty() {
        if (loginView != null) {
            loginView.hideProgress();

            loginView.setFieldsEmpty();
        }
    }

    @Override
    public void onSuccess(LoginResponse loginResponse) {
        if (loginView != null) {
            loginView.hideProgress();

            loginView.navigateToHome(loginResponse);
        }
    }

    @Override
    public void onResponseError(String errorMsg) {
        if (loginView != null) {
            loginView.hideProgress();

            loginView.setResponseError(errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        loginView = null;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}