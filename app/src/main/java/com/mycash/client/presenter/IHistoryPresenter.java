package com.mycash.client.presenter;

/**
 * Created by Narine on 16,April,2018
 */
public interface IHistoryPresenter {

    void getPaymentsHistoryData ();

    void onDestroy();
}
