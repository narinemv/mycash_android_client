package com.mycash.client.presenter;

public interface ILoginPresenter {

    void validateCredentials(String ccc, String phone, String password);

    void onDestroy();
}