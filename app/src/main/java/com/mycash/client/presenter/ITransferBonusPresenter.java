package com.mycash.client.presenter;

/**
 * Created by Narine on 02,July,2018
 */
public interface  ITransferBonusPresenter {

    void validateCredentials(String branchNumber, String bonus,String branchBonus, String phone, String buyerPhone);

    void validatePhone(String phone);

    void validateBranchNumber(String branchNumber);

    void onDestroy();
}
