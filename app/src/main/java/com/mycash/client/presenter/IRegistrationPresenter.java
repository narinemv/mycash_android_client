package com.mycash.client.presenter;

public interface IRegistrationPresenter {

    void validateRegistrationCredentials(String ccc,
                                         String firstName,
                                         String lastName,
                                         String email,
                                         String phone,
                                         String password,
                                         String confirmPassword);

    void validateCode (String code);

    void onDestroy();
}