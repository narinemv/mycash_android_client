package com.mycash.client.presenter;

import android.text.TextUtils;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.ITransferBonusInteractor;
import com.mycash.client.model.TransferBonusInteractorImpl;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.ui.view.ITransferBonusView;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Narine on 02,July,2018
 */
public class TransferBonusPresneterImpl implements ITransferBonusPresenter, ITransferBonusInteractor.OnTransferBonusFinishedListener,
                                                                            ITransferBonusInteractor.OnGetBranchFinishedListener,
                                                                            ITransferBonusInteractor.OnGetUserFinishedListener{


    private NetworkService service;
    private ITransferBonusView transferBonusView;
    private ITransferBonusInteractor listener;
    private CompositeDisposable compositeDisposable;
    private BuyerBonusResponse buyerBonusResponse;
    private CheckOtherBuyerStateResponse checkOtherBuyerStateResponse;

    public TransferBonusPresneterImpl(ITransferBonusView transferBonusView, NetworkService service, TransferBonusInteractorImpl listener) {
        this.transferBonusView = transferBonusView;
        this.listener = listener;
        this.service = service;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void validateCredentials(String branchNumber, String bonus, String branchBonus, String phone, String buyerPhone) {
        if (transferBonusView != null) {
            transferBonusView.showProgress();
        }

        if (TextUtils.isEmpty(branchNumber)) {
            onBranchNumberError();
            return;
        }

        if (TextUtils.isEmpty(bonus)) {
            onBonusError();
            return;
        }

        if (TextUtils.isEmpty(phone)) {
            onPhoneError();
            return;
        }

        if (Integer.valueOf(branchBonus) == 0) {
            onBonusError(MyCashClientConstants.TRANSFER_ERROR_TYPE.NO_BONUS);
            return;
        }

        //Date date = java.util.Calendar.getInstance().getTime();
        if (buyerBonusResponse == null) {
            onBranchNumberError();
            return;
        }
        if (checkOtherBuyerStateResponse == null){
            onPhoneError();
            return;
        }
            TransferBonus request = new TransferBonus();
            request.setAmount(Integer.valueOf(bonus));
            //request.setDate(date);
            request.setFromBuyerId(buyerBonusResponse.getBuyerId());
            request.setFromBuyerPhone(buyerPhone);
            request.setStoreId(buyerBonusResponse.getStore().getId());
            request.setToBuyerId(checkOtherBuyerStateResponse.getBuyerId());
            request.setUserId(buyerBonusResponse.getStore().getUserId());
            request.setToBuyerPhone(phone);

            listener.transfer(request, this, service, compositeDisposable);
    }

    @Override
    public void validatePhone(String phone) {
        if( transferBonusView != null){
            transferBonusView.showProgress();
        }

        if( TextUtils.isEmpty(phone)){
            onPhoneError();
            return;
        }

        Request request = new Request();
        request.setValue(phone);

        listener.getUserByPhone(request, this , service, compositeDisposable);
    }

    @Override
    public void validateBranchNumber(String branchNumber) {
        if( transferBonusView != null){
            transferBonusView.showProgress();
        }

        if( TextUtils.isEmpty(branchNumber)){
            onBranchNumberError();
            return;
        }

        Request request = new Request();
        request.setValue(branchNumber);

        listener.getBranchByNumber(request, this , service, compositeDisposable);
    }

    @Override
    public void onBranchNumberError() {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setBranchNumberError();
        }
    }

    @Override
    public void onBonusError() {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setBonusError();
        }
    }

    @Override
    public void onBonusError(MyCashClientConstants.TRANSFER_ERROR_TYPE error) {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setBonusError(error);
        }
    }

    @Override
    public void onPhoneError() {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setPhoneError();
        }
    }

    @Override
    public void onTransferSuccess(TransferBonus transferBonus) {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonus.setUserName(checkOtherBuyerStateResponse.getBuyerFirstName() + " " +checkOtherBuyerStateResponse.getBuyerLastName());
            transferBonusView.navigateToFinishPage(transferBonus);
        }
    }

    @Override
    public void onTransferResponseError(String errorMsg) {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setTransferResponseError(errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        transferBonusView = null;
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
    }

    @Override
    public void onGetBranchSuccess(BuyerBonusResponse branchResponse) {
        this.buyerBonusResponse = branchResponse;

        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.branchNumberValidationSuccess(branchResponse);
        }
    }

    @Override
    public void onGetBranchResponseError(String errorMsg) {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setBranchValidationResponseError(errorMsg);
            this.buyerBonusResponse = null;
        }
    }

    @Override
    public void onGetUserSuccess(CheckOtherBuyerStateResponse response) {
        this.checkOtherBuyerStateResponse = response;

        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.phoneValidationSuccess(response);
        }
    }

    @Override
    public void onGetUserResponseError(String errorMsg) {
        if (transferBonusView != null){
            transferBonusView.hideProgress();

            transferBonusView.setPhoneValidationResponseError(errorMsg);
            this.checkOtherBuyerStateResponse = null;
        }
    }
}
