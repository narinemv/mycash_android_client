package com.mycash.client.presenter;

/**
 * Created by Narine on 04,April,2018
 */
public interface IPayInCashPresenter {

    void loadRxData(String branchId);

    void rxUnSubscribe();

    void onDestroy();

    void calculateCash(int amount, int bonus, boolean isBonusSwitched);

    void generateQRImage(String qrContent);
}
