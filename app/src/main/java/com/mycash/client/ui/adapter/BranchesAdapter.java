package com.mycash.client.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.BranchResponse;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Narine on 12,June,2018
 */
public class BranchesAdapter extends RecyclerView.Adapter<BranchesAdapter.BranchesViewHolder>{

    private Context context;
    private List<BranchResponse> branchesList;
    private ItemClickListener clickListener;
    private String baseURL;

    public interface ItemClickListener {
       void onItemClick(View view, int position);
    }

    public BranchesAdapter(Context context, List<BranchResponse> branchesList, String baseURL) {
        this.branchesList = branchesList;
        this.context = context;
        this.baseURL = baseURL;
    }

    @Override

    public BranchesAdapter.BranchesViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.branch_item, null);
        return new BranchesAdapter.BranchesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BranchesAdapter.BranchesViewHolder branchesViewHolder, int position) {
        BranchResponse branch = branchesList.get(position);

        branchesViewHolder.branchTitleTextView.setText(branch.getName());
        branchesViewHolder.branchTitleTextView.setTypeface(branchesViewHolder.branchTitleTextView.getTypeface(), Typeface.BOLD);

        //branchesViewHolder.branchSaleValueTextView.setText(branch.getDiscount()+"%");
        branchesViewHolder.branchCashbackValueTextView.setText(branch.getBonus1()+"%");
        //branchesViewHolder.branchBonusValueTextView.setText(branch.getBonus1()+"%");

        if(branch.getImageUrl() != null && !branch.getImageUrl().isEmpty()) {
            List<String> image_urls = Arrays.asList(branch.getImageUrl().split(","));
            Picasso.with(context).setLoggingEnabled(true);
            Picasso.with(context).load(baseURL + MyCashClientConstants.LOGO_API + branch.getUserId())
                    .resize(300,200)
                    .into(branchesViewHolder.branchImage);
           /* Picasso.with(context).load(Utils.getImagesBaseUrl(context) + image_urls.get(0))
                    .resize(300, 200)
                    .into(branchesViewHolder.branchImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) branchesViewHolder.branchImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imageBitmap);
                            //imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(15f);
                            branchesViewHolder.branchImage.setImageDrawable(imageDrawable);
                        }
                        @Override
                        public void onError() {
                            branchesViewHolder.branchImage.setImageResource(R.drawable.noimage);
                        }
                    });*/
        }else {
            branchesViewHolder.branchImage.setImageResource(R.drawable.noimage);
        }
    }

    @Override
    public int getItemCount() {
        return branchesList == null ? 0 : branchesList.size();
    }

    protected class BranchesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.branchTitleTextView)
        TextView branchTitleTextView;

        @BindView(R.id.branchSaleValueTextView)
        TextView branchSaleValueTextView;

        @BindView(R.id.branchCashbackValueTextView)
        TextView branchCashbackValueTextView;

        @BindView(R.id.branchBonusValueTextView)
        TextView branchBonusValueTextView;

        @BindView(R.id.branchImage)
        PorterShapeImageView branchImage;

        public BranchesViewHolder(View view) {
            super(view);

            // binding view
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }

    }

    public void setClickListener(BranchesAdapter.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

}
