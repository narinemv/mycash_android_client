package com.mycash.client.ui.view;

import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.User;

public interface IRegisterView {

    void showProgress();

    void hideProgress();

    void setFirstNameEmpty();

    void setLastNameEmpty();

    void setPhoneEmpty();

    void setEmailEmpty();

    void setPasswordEmpty();

    void setConfirmPasswordEmpty();

    void setFieldsEmpty();

    void setEmailNonValid();

    void setPswdNotConfirmed();

    void navigateToHome(LoginResponse response);

    void setRegistrationResponseError(String responseError);

    void setCodeEmpty();

    void goToLoginPage(LoginResponse response);

    void setValidationResponseError(String responseError);

    void showNoConnectionMessage();
}