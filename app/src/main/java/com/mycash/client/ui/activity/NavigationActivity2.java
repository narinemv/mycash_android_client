package com.mycash.client.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.ui.fragments.CategoryFragment;
import com.mycash.client.ui.fragments.HistoryFragment;
import com.mycash.client.ui.fragments.MapFragment;
import com.mycash.client.ui.fragments.PaymentMenuFragment;
import com.mycash.client.ui.fragments.SettingsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NavigationActivity2 extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @Nullable
    @BindView(R.id.message)
    TextView mTextMessage;

    @Nullable
    @BindView(R.id.navigation)
    public BottomNavigationView navigation;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_navigation2);

        unbinder = ButterKnife.bind(this);

        navigation.setOnNavigationItemSelectedListener(this);
        //Utils.disableShiftMode(navigation);

        goToFragment(new PaymentMenuFragment());
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {

        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.navigation_pay:
                 fragment = new PaymentMenuFragment();
                 goToFragment(fragment);
                 return true;
            case R.id.navigation_category:
                 fragment = new CategoryFragment();
                 goToFragment(fragment);
                 return true;
            case R.id.navigation_map:
                 fragment = new MapFragment();
                 goToFragment(fragment);
                 return true;
            case R.id.navigation_history:
                 fragment = new HistoryFragment();
                 goToFragment(fragment);
                 return true;
            case R.id.navigation_settings:
                 fragment = new SettingsFragment();
                 goToFragment(fragment);
                 return true;
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        if( unbinder != null)
            unbinder.unbind();
        super.onDestroy();
    }
}
