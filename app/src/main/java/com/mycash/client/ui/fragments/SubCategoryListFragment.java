package com.mycash.client.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.model.entity.SubCategory;
import com.mycash.client.ui.activity.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SubCategoryListFragment extends ListFragment {

    @BindView(R.id.categoryImageView)
    ImageView categoryImageView;

    @BindView(R.id.categoryNameTxt)
    TextView categoryNameTxt;

    private List<SubCategory> subCategoryList;
    private String[] categoryNameList;
    private String subCategoryName;
    private String categoryName;
    private String categoryImage;
    private String categoryColor;
    private Unbinder unbinder;

    public SubCategoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            subCategoryList = (List<SubCategory>) getArguments().getSerializable(MyCashClientConstants.SUB_CATEGORY_LIST);
            categoryName = getArguments().getString(MyCashClientConstants.CATEGORY_NAME);
            categoryImage = getArguments().getString(MyCashClientConstants.CATEGORY_IMAGE);
            categoryColor = getArguments().getString(MyCashClientConstants.CATEGORY_COLOR);
        }

        categoryNameList = new String[subCategoryList.size()];
        for (int i = 0; i < subCategoryList.size(); i++) {
            categoryNameList[i] = subCategoryList.get(i).getName();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar("", true, false);
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sub_category_list, container, false);

        unbinder = ButterKnife.bind(this, view);

        // We need to use a different list item layout for devices older than Honeycomb
        int layout = android.R.layout.simple_list_item_1;

        // Create an array adapter for the list view, using the Ipsum headlines array
        setListAdapter(new ArrayAdapter<String>(getActivity(), layout, categoryNameList));

        categoryNameTxt.setText(categoryName);
        categoryNameTxt.setTextColor(Color.parseColor(categoryColor));
        int drawableResourceId = getActivity().getResources().getIdentifier(categoryImage, "drawable", getActivity().getPackageName());
        categoryImageView.setImageResource(drawableResourceId);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        subCategoryName = categoryNameList[position];
        goToBranchesList(new BranchesFragment(), String.valueOf(subCategoryList.get(position).getNumber()));
    }

    public void goToBranchesList(Fragment fragment, String categoryId) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putString(MyCashClientConstants.SUB_CATEGORY_NUM, categoryId);
        args.putString(MyCashClientConstants.SUB_CATEGORY_NAME, subCategoryName);

        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }
}
