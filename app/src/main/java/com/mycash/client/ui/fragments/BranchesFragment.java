package com.mycash.client.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.adapter.BranchesAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BranchesFragment extends Fragment implements BranchesAdapter.ItemClickListener {

    @BindView(R.id.branchesRecyclerView)
    RecyclerView branchesRecyclerView;

    @BindView(R.id.branchesNoData)
    TextView branchesNoData;

    private Unbinder unbinder;
    private int numberOfColumns = 2;
    private String subCategoryName = "";
    private BranchesAdapter branchesAdapter;
    private CompositeDisposable compositeDisposable;
    private List<BranchResponse> branchResponse = new ArrayList<>();

    public BranchesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String categoryId = "";
        if (getArguments() != null) {
            categoryId = getArguments().getString(MyCashClientConstants.SUB_CATEGORY_NUM);
            subCategoryName = (String) getArguments().get(MyCashClientConstants.SUB_CATEGORY_NAME);
        }

        ((BaseActivity)getActivity())._showProgressBar();
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(MyCashClientApplication.getNetworkService().getAPI().getBranchesByCategoryId(categoryId, ((BaseActivity) getActivity()).createLocationRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(buyerBonusResponses -> handleResponse(buyerBonusResponses), error -> handleError(error)));
    }

    private void handleResponse(List<BranchResponse> branchResponse) {
        ((BaseActivity)getActivity())._hideProgressBar();
        if(branchResponse != null && branchResponse.size() > 0) {
            this.branchResponse.addAll(branchResponse);
            branchesAdapter.notifyDataSetChanged();
        }else{
            branchesRecyclerView.setVisibility(View.GONE);
            branchesNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar(subCategoryName, true, false);
    }

    private void handleError(Throwable  error) {
        ((BaseActivity)getActivity())._hideProgressBar();
        ((BaseActivity)getActivity())._showError(MyCashClientApplication.getNetworkService().getErrorMessage(error));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_branches, container, false);

        unbinder = ButterKnife.bind(this, view);

        branchesRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));

        branchesAdapter = new BranchesAdapter(getActivity(), branchResponse, MyCashClientApplication.getNetworkService().getBaseUrl());
        branchesAdapter.setClickListener(this);
        branchesRecyclerView.setAdapter(branchesAdapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
        super.onDestroyView();
    }

    @Override
    public void onItemClick(View view, int position) {
        ((BaseActivity)getActivity()).goToBranchDetailsFragment(new BranchDetailsFragment(), branchResponse, position);
    }
}
