package com.mycash.client.ui.view;

import android.graphics.Bitmap;

import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.model.entity.BuyerBonusResponse;

/**
 * Created by Narine on 04,April,2018
 */
public interface IPayInCashView {

    void showProgress();

    void hideProgress();

    void showNoConnectionMessage();

    void showBuyerBonusResponseResults(BuyerBonusResponse response);

    void showBuyerBonusResponseFailure(String error);

    void showCash(String cash);

    void passQRImage(Bitmap bitmap);
}

