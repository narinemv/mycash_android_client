package com.mycash.client.ui.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.ExtendedViewPager;
import com.mycash.client.helper.TouchImageView;
import com.mycash.client.helper.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ViewPagerActivity extends BaseActivity {

    private static ArrayList<String> imageUrlList;
    private static String baseUrl;

    @BindView(R.id.view_pager)
    ExtendedViewPager viewPageriew;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        imageUrlList = getIntent().getStringArrayListExtra(MyCashClientConstants.IMAGE_URLS);
        baseUrl = Utils.getImagesBaseUrl(getApplicationContext());

        ButterKnife.bind(this);

        setupActionBar( "", false, true);

        viewPageriew.setAdapter(new TouchImageAdapter(this));
    }

    static class TouchImageAdapter extends PagerAdapter {

        private Activity mActivity;

        public  TouchImageAdapter (Activity activity){
            mActivity = activity;
        }

        @Override
        public int getCount() {
            return imageUrlList.size();
        }

        @Override
        public View instantiateItem(final  ViewGroup container, int position) {
            final TouchImageView img = new TouchImageView(container.getContext());


            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    img.setImageBitmap(bitmap);
                    container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {}

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {}
            };
            img.setTag(target);
            Picasso.with(mActivity).load(baseUrl + imageUrlList.get(position)).into(target);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

    public static boolean isPause = false;
    @Override
    public void onPause() {
        super.onPause();
        isPause = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        isPause = false;
    }
}
