package com.mycash.client.ui.fragments;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mycash.client.R;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.presenter.IHistoryPresenter;
import com.mycash.client.ui.activity.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HistoryFragment extends Fragment {

    @Nullable
    @BindView(R.id.historyTabs)
    public TabLayout historyTabs;

/*  @Nullable
    @BindView(R.id.historViewpager)
    ViewPager historViewpager;*/

    private IHistoryPresenter presenter;
    private List<PaymentHistory> historyList;
    private Unbinder unbinder;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        unbinder = ButterKnife.bind(this, view);

        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.history), false, false);

        setupTabs();
        tablayoutListenerSetup();

        goToHistoryFragments(new PaymentHistoryFragment());
        return view;
    }

    private void tablayoutListenerSetup() {
        /* ViewPagerAdapter adapter = null;
        if(adapter == null) {
            adapter = new ViewPagerAdapter
                    (getSupportFragmentManager(), historyTabs.getTabCount());
            historViewpager.setAdapter(adapter);
            historViewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(historyTabs)); */
            historyTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    // historViewpager.setCurrentItem(tab.getPosition());
                    if (historyTabs.getSelectedTabPosition() == 0) {
                        goToHistoryFragments(new PaymentHistoryFragment());
                    } else if (historyTabs.getSelectedTabPosition() == 1) {
                        goToHistoryFragments(new TransferHistoryFragment());
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {}

                @Override
                public void onTabReselected(TabLayout.Tab tab) {}
            });
        //}
    }

    public void goToHistoryFragments(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        transaction.add(R.id.history_fragment_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    private void setupTabs() {
        historyTabs.addTab(historyTabs.newTab().setText(getString(R.string.payment)));
        historyTabs.addTab(historyTabs.newTab().setText(getString(R.string.transfer)));
        LinearLayout linearLayout = (LinearLayout)historyTabs.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.WHITE);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(20);
        linearLayout.setDividerDrawable(drawable);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.history), false, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        super.onDestroyView();
    }
}
