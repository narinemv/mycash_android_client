package com.mycash.client.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beautycoder.pflockscreen.PFFLockScreenConfiguration;
import com.beautycoder.pflockscreen.activity.PFLockScreenActivity;
import com.beautycoder.pflockscreen.security.PreferencesSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.LocaleManager;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.ui.fragments.CategoryFragment;
import com.mycash.client.ui.fragments.HistoryFragment;
import com.mycash.client.ui.fragments.MapFragment;
import com.mycash.client.ui.fragments.PayInCashFragment;
import com.mycash.client.ui.fragments.PaymentMenuFragment;
import com.mycash.client.ui.fragments.SettingsFragment;
import com.mycash.client.ui.fragments.ShowMessageFragment;
import com.mycash.client.ui.fragments.TransferBonusFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity implements  OnMapReadyCallback,
                                                                GoogleApiClient.ConnectionCallbacks,
                                                                GoogleApiClient.OnConnectionFailedListener,
                                                                com.google.android.gms.location.LocationListener {
    
    public static final String HISTORY_ITEM = "history_item";
    public static final String PAYMENT_ID = "payment_id";
    public static final String TRANSFER_HISTORY_ITEM = "transfer_history_item";
    public static final String TRANSFER_ID = "transfer_id";
    public int distance = 20;
    private static final MyBroadcastReceiver mChangeConnectionReceiver = new MyBroadcastReceiver();
    private static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private ConnectionReceiverListener connectionReceiverListener;
    private boolean haveInternet;
    private ProgressBar progressBar;
    private Activity mActivity;
    //MAP
    private GoogleMap map;
    private Location location;
    public  GoogleApiClient googleApiClient;
    private LocationManager locationManager;
    private static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    private static final int LOCATION_UPDATE_MIN_TIME = 1000;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(LocaleManager.setLocale(base)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mActivity = this;
        haveInternet = ConnectionHelper.haveInternet(this, true);

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (this instanceof NavigationActivity2) {
            setUpGClient();
        }
    }

    public int getColNumDisplay() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int num = 2;
        switch(metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                Log.v("", "::DENSITY_LOW");
                break;

            case DisplayMetrics.DENSITY_MEDIUM:
                Log.v("", "::DENSITY_MEDIUM");
                break;

            case DisplayMetrics.DENSITY_HIGH:
                Log.v("", "::DENSITY_HIGH");
                break;

            case DisplayMetrics.DENSITY_XHIGH:
                Log.v("", "::DENSITY_XHIGH");
                num = 3;
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                Log.v("", "::DENSITY_XXHIGH");
                num = 3;
                break;

            case DisplayMetrics.DENSITY_XXXHIGH:
                Log.v("", "::DENSITY_XXXHIGH");
                num = 4;
                break;
        }
        return  num;
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(BaseActivity.this)
                .enableAutoManage(BaseActivity.this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
       setLocation(location);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
         map = googleMap;
    }

    public interface ConnectionReceiverListener {
         void onConnectionReturned();
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                }
            }
        }else{
            getMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    @Override
    protected void onDestroy() {
        if( googleApiClient != null) {
            googleApiClient.stopAutoManage(BaseActivity.this);
            googleApiClient.disconnect();
        }
        super.onDestroy();
    }

    final static class MyBroadcastReceiver extends BroadcastReceiver {
        public List<ConnectionReceiverListener> listeners = new ArrayList<>();

        public void addListener(ConnectionReceiverListener listener) {
            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }

        public void removeListener(ConnectionReceiverListener listener) {
            if (listeners.contains(listener)) {
                listeners.remove(listener);
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (CONNECTIVITY_CHANGE_ACTION.equals(action)) {
                //check internet connection
                if (!ConnectionHelper.isConnectedOrConnecting(context)) {
                    if (context != null) {
                        boolean show = false;
                        if (ConnectionHelper.lastNoConnectionTs == -1) {//first time
                            show = true;
                            ConnectionHelper.lastNoConnectionTs = System.currentTimeMillis();
                        } else {
                            if (System.currentTimeMillis() - ConnectionHelper.lastNoConnectionTs > 10) {
                                show = true;
                                ConnectionHelper.lastNoConnectionTs = System.currentTimeMillis();
                            }
                        }

                        if (show) {
                            showToast(context, false);
                            ConnectionHelper.isOnline = false;
                        }
                    }
                } else {
                    if (!ConnectionHelper.isOnline) {
                        showToast(context, true);
                        ConnectionHelper.isOnline = true;
                        for (ConnectionReceiverListener listener : listeners) {
                            listener.onConnectionReturned();
                        }
                    }
                }
            }
        }

        // Showing the status in a Toast
        private void showToast(Context context, boolean isConnected) {
            String message = context.getString(isConnected ? R.string.connession_ok : R.string.connession_ko);

            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    boolean isNavigationActivityPaused = false;
    @Override
    protected void onPause() {
        super.onPause();
        if (mActivity instanceof NavigationActivity2){
            isNavigationActivityPaused = true;
        }
        if (mChangeConnectionReceiver != null) {
            mChangeConnectionReceiver.removeListener(connectionReceiverListener);
            connectionReceiverListener = null;
            this.unregisterReceiver(mChangeConnectionReceiver);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //this lock fragment orintation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        connectionReceiverListener = new ConnectionReceiverListener() {
            @Override
            public void onConnectionReturned() {
                haveInternet = true;
                connectionReturned();
            }
        };
        mChangeConnectionReceiver.addListener(connectionReceiverListener);
        if (!haveInternet) {
            haveInternet = ConnectionHelper.haveInternet(this, true);
            if (haveInternet) {
                connectionReturned();
            }
        }
        registerReceiver();
        //detect and show pin lock screen in case if it opens from background
        createPinLockScreen();
    }

    private void createPinLockScreen () {
        if (ViewPagerActivity.isPause){
            isNavigationActivityPaused = false;
            ViewPagerActivity.isPause = false;
        }

        if((Utils.getToken(this) != null && !Utils.getToken(this).isEmpty())&&
                (PreferencesSettings.getCode(this) != null && !PreferencesSettings.getCode(this).isEmpty())){
            if (isNavigationActivityPaused && !PFLockScreenActivity.active) {
                createPinLockScreen(false);
                isNavigationActivityPaused = false;
            }
        }
    }
//detect and show pin lock screen in case if it opens from background
    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mChangeConnectionReceiver, intentFilter);
    }

    protected void connectionReturned() {
        restartActivity();
    }

    protected void restartActivity() {
        startActivity(getIntent());
        finish();
    }

    /**
     *
     * @param classObject
     */
    public void goToActivity(Class<?> classObject) {
        Intent intent = new Intent(this, classObject);
        startActivity(intent);
    }

    public void _showProgressBar() {
        View rootView = this.getLayoutInflater().inflate(R.layout.activity_base, getWindow().getDecorView().findViewById(android.R.id.content));

        progressBar = rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void _hideProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void _showError(String error){
        int error_id = getResources().getIdentifier(error, "string", getPackageName());
        try {
            error = getString(error_id);
        }catch (Exception ex){
            error = getResources().getString(R.string.msg_something_error);
        }

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public void setupActionBar(String titlelName, boolean isShowBackButton, boolean isShowCloseButton) {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar == null) return;
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.custom_action_bar);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        View view = actionBar.getCustomView();
        if(view == null) return;
        Toolbar parent = (Toolbar) view.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        TextView titleTxtView = view.findViewById(R.id.titleTextView);
        titleTxtView.setText(titlelName);
        titleTxtView.setTypeface(titleTxtView.getTypeface(), Typeface.BOLD);

        if( isShowBackButton){
            findViewById(R.id.backImageView).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.backImageView).setVisibility(View.INVISIBLE);
        }

        if (isShowCloseButton){
            findViewById(R.id.closeImageView).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.closeImageView).setVisibility(View.INVISIBLE);
        }

        findViewById(R.id.backImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.closeImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( mActivity instanceof RegisterActivity){
                    mActivity.finish();
                } else if (mActivity instanceof ViewPagerActivity){
                    mActivity.finish();
                } else {
                   goToFragment(new PaymentMenuFragment());
                }
            }
        });
    }

    long lastClick;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment fragment = null;

                if ((getSupportFragmentManager().findFragmentByTag(MapFragment.class.getSimpleName()) != null && getSupportFragmentManager().findFragmentByTag(MapFragment.class.getSimpleName()).isVisible())
                   || (getSupportFragmentManager().findFragmentByTag(HistoryFragment.class.getSimpleName()) != null && getSupportFragmentManager().findFragmentByTag(HistoryFragment.class.getSimpleName()).isVisible())
                   || (getSupportFragmentManager().findFragmentByTag(CategoryFragment.class.getSimpleName()) != null && getSupportFragmentManager().findFragmentByTag(CategoryFragment.class.getSimpleName()).isVisible())
                   || (getSupportFragmentManager().findFragmentByTag(PaymentMenuFragment.class.getSimpleName()) != null && getSupportFragmentManager().findFragmentByTag(PaymentMenuFragment.class.getSimpleName()).isVisible())
                   || (getSupportFragmentManager().findFragmentByTag(SettingsFragment.class.getSimpleName()) != null && getSupportFragmentManager().findFragmentByTag(SettingsFragment.class.getSimpleName()).isVisible())) {

                    long now = System.currentTimeMillis();
                    if (now - lastClick < 2000) {
                        //super.onBackPressed();
                        moveTaskToBack(true);
                    } else {
                        lastClick = now;
                        //ToastUtil.makeToastAndShow(this, R.string.click_again_to_exit, Toast.LENGTH_SHORT);
                        Toast.makeText(getApplicationContext(), "Please, click again to exit", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    super.onBackPressed();
                }

                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void goToFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment instanceof PayInCashFragment) {
            transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        } else if(fragment instanceof TransferBonusFragment) {
            transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        } else if(fragment instanceof ShowMessageFragment) {
            transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        }

        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commitAllowingStateLoss();
    }

    public void goToBranchDetailsFragment (Fragment fragment, List<BranchResponse> branchList, int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putSerializable(MyCashClientConstants.BRANCHES_LIST, (Serializable) branchList);
        args.putInt(MyCashClientConstants.SELECTED_BRANCH_NUM, position);

        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void goToUserProfileFragment (Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MapFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, intent);
        }

        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        //finish();
                        break;
                }
                break;
        }
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);

                    SettingsClient client = LocationServices.getSettingsClient(BaseActivity.this);
                    Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());


                    task.addOnSuccessListener(BaseActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            // All location settings are satisfied. The client can initialize
                            // location requests here.
                            int permissionLocation = ContextCompat
                                    .checkSelfPermission(BaseActivity.this,
                                            Manifest.permission.ACCESS_FINE_LOCATION);
                            if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, locationListener);
                                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                                if (map != null) {
                                    map.setMyLocationEnabled(true);
                                }

                                if (location != null) {
                                    Log.d(MyCashClientConstants.TAG, "::lat/long" + location.getLatitude()+ "/" + location.getLongitude());

                                    setLocation(location);
                                    Utils.saveLocation(location, getApplicationContext());
                                }
                            }
                        }
                    });

                    task.addOnFailureListener(BaseActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@io.reactivex.annotations.NonNull Exception e) {
                            if (e instanceof ResolvableApiException) {
                                // Location settings are not satisfied, but this can be fixed
                                // by showing the user a dialog.
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    ResolvableApiException resolvable = (ResolvableApiException) e;
                                    resolvable.startResolutionForResult(BaseActivity.this,
                                            REQUEST_CHECK_SETTINGS_GPS);
                                } catch (IntentSender.SendIntentException sendEx) {
                                    // Ignore the error.
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                Log.d(MyCashClientConstants.TAG, "::lat/long " + location.getLatitude() + " / " + location.getLongitude());

                setLocation(location);
                Utils.saveLocation(location, getApplicationContext());

                locationManager.removeUpdates(locationListener);
            } else {
                Log.d(MyCashClientConstants.TAG, "Location is null");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {}

        @Override
        public void onProviderEnabled(String s) {}

        @Override
        public void onProviderDisabled(String s) {}
    };


    public com.mycash.client.model.entity.LocationRequest createLocationRequest () {
        com.mycash.client.model.entity.LocationRequest request = new com.mycash.client.model.entity.LocationRequest();
        String distance = Utils.getDistance(this);
        if (distance != null) {
            this.distance = Integer.valueOf(distance);
        }

        double latitude = 1.1;
        double longitude = 1.1;
        if( Utils.getLatitude(this) > -1 && Utils.getLongitude(this) > -1){
            latitude = Utils.getLatitude(this);
            longitude = Utils.getLongitude(this);
        }
        request.setLatitude(latitude);
        request.setLongitude(longitude);
        request.setDistance(this.distance);

        return request;
    }

    public void closeKeyboard (View view){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //Manage Lock Screen
    //PFLockScreenFragment fragment;
    public void createPinLockScreen(boolean isChangePin) {

        String title = (isChangePin)? "Set Pin" : "Create Pin";
        PFFLockScreenConfiguration.Builder builder = new PFFLockScreenConfiguration.Builder(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.removeToken(getApplicationContext());
                MyCashClientApplication.getNetworkService().removeToken();
                PreferencesSettings.removePin(getApplicationContext());

                goToActivity(LoginActivity.class);
                mActivity.finish();
            }
        }, this)
                .setTitle(title)
                .setCodeLength(4)
                .setLeftButton(getString(R.string.log_out))
                .setUseFingerprint(false);

        builder.setMode(!isChangePin
                ? PFFLockScreenConfiguration.MODE_AUTH
                : PFFLockScreenConfiguration.MODE_CREATE);

        Intent intent = new Intent(this, PFLockScreenActivity.class);
        intent.putExtra("builder", builder);
        startActivity (intent);
    }

    public static String generateRandomChars(int length) {
        String candidateChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }

    public  String decryptedString(String encrepted) {
        String infoStr = generateRandomChars(1);
        String infoStrEncoded = android.util.Base64.encodeToString(encrepted.getBytes(), android.util.Base64.NO_WRAP);
        String infoStrEncodedMix = "";
        for (int i = 0; i < infoStrEncoded.length(); i+=2) {
            infoStrEncodedMix += generateRandomChars(1);
        }

        Log.i(MyCashClientConstants.TAG, "::infoStrEncodedMix : " + infoStrEncodedMix);
        return "";
    }
}
