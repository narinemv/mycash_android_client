package com.mycash.client.ui.fragments;

import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.TransferHistoryDetailsPresenterImpl;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.model.entity.TransferHistoryDetails;
import com.mycash.client.presenter.ITransferHistoryDetailsPresenter;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.view.ITransferHistoryDetailsView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.mycash.client.ui.activity.BaseActivity.TRANSFER_HISTORY_ITEM;
import static com.mycash.client.ui.activity.BaseActivity.TRANSFER_ID;

public class TransferHistoryDetailsFragment extends Fragment implements ITransferHistoryDetailsView{

    @BindView(R.id.transfer_details_order_id)
    TextView transferDetailsOrderId;

    @BindView(R.id.transfer_details_store_number)
    TextView transferDetailsStoreNumber;

    @BindView(R.id.transfer_details_datetime)
    TextView transferDetailsDatetime;

    @BindView(R.id.transfer_details_bonus)
    TextView transferDetailsBonus;

    @BindView(R.id.transfer_details_getter_phone)
    TextView transferDetailsGetterPhone;

    @BindView(R.id.transfer_details_getter_surname)
    TextView transferDetailGetterSurName;

    @BindView(R.id.transfer_details_getter_name)
    TextView transferDetailsGetterName;

    @BindView(R.id.transfer_details_branchName)
    TextView transferDetailsBranchName;

    @BindView(R.id.transfer_details_address)
    TextView transferDetailsAddress;

    @BindView(R.id.transfer_details_header)
    LinearLayout transferDetailsHeader;

    @BindColor(R.color.colorAccent)
    int colorValid;

    private Unbinder unbinder;
    private String transferId;
    private ITransferHistoryDetailsPresenter presenter;
    private TransferHistoryDetails details;
    private TransferBonus transferData;

    public TransferHistoryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            transferData = (TransferBonus) getArguments().getSerializable(TRANSFER_HISTORY_ITEM);
            transferId = (String) getArguments().get(TRANSFER_ID);
        }

        presenter = new TransferHistoryDetailsPresenterImpl(this, MyCashClientApplication.getNetworkService());

        if ( ConnectionHelper.isConnected(getActivity())) {
             presenter.getTransferredHistoryDetails(transferId);
        } else {
            showNoConnectionMessage();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transfer_history_details, container, false);

        unbinder = ButterKnife.bind(this, view);

        ((BaseActivity)getActivity()).setupActionBar(getString(R.string.history_details), true, false);

        transferDetailsBranchName.setTypeface(transferDetailsBranchName.getTypeface(), Typeface.BOLD);

        return view;
    }

    private void setupUI() {
        if (transferData != null) {
            transferDetailsOrderId.setText(String.valueOf(transferData.getId()));
            transferDetailsStoreNumber.setText(String.valueOf(transferData.getStoreId()));
            transferDetailsDatetime.setText(Utils.changeDateFormat(transferData.getDate()));
            transferDetailsGetterName.setText(details.getDealWithBuyer().getFirstName());
            transferDetailGetterSurName.setText(details.getDealWithBuyer().getLastName());
            transferDetailsGetterPhone.setText(transferData.getToBuyerPhone());
            transferDetailsBonus.setText(String.valueOf(transferData.getAmount()));
            transferDetailsBranchName.setText(details.getStore().getName());
            transferDetailsAddress.setText(details.getStore().getAddress());
        }
        ((GradientDrawable) transferDetailsHeader.getBackground()).setColor(colorValid);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        if(presenter != null)
           presenter.onDestroy();

        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(HistoryFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onResume();
        }

        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void showTransferHistoryDetails(TransferHistoryDetails transferDetails) {
        this.details = transferDetails;

        setupUI();
    }

    @Override
    public void showTransferHistoryDetailsResponseError(String error) {
        //((BaseActivity)getActivity())._showError(error);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }
}
