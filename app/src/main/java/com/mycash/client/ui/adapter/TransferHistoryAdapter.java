package com.mycash.client.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.model.entity.TransferBonus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Narine on 17,April,2018
 */
public class TransferHistoryAdapter extends RecyclerView.Adapter<TransferHistoryAdapter.TransferHistoryViewHolder> {

    private Context context;
    private List<TransferBonus> transferList;
    private TransferHistoryAdapter.ItemClickListener clickListener;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public TransferHistoryAdapter(Context context, List<TransferBonus> transferList) {
        this.transferList = transferList;
        this.context = context;
    }

    @Override
    public TransferHistoryAdapter.TransferHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transfer_history_item, null);
        return new TransferHistoryAdapter.TransferHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TransferHistoryAdapter.TransferHistoryViewHolder transferHistoryViewHolder, int position) {
        TransferBonus transferItem = transferList.get(position);

        transferHistoryViewHolder.transferItemDate.setText(Utils.changeDateFormat(transferItem.getDate()));
        transferHistoryViewHolder.transferItemUser.setText(String.valueOf(transferItem.getStoreId()));

        String sign = (Utils.getPhoneNumber(context).equalsIgnoreCase(transferItem.getFromBuyerPhone()))? "-" : "+";
        transferHistoryViewHolder.transferItemBonus.setText(sign + String.valueOf(transferItem.getAmount()));
    }

    @Override
    public int getItemCount() {
        return transferList == null ? 0 : transferList.size();
    }

    protected class TransferHistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.transfer_item_date)
        TextView transferItemDate;

        @BindView(R.id.transfer_item_user)
        TextView transferItemUser;

        @BindView(R.id.transfer_item_bonus)
        TextView transferItemBonus;

        public TransferHistoryViewHolder(View view) {
            super(view);

            // binding view
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public  void setClickListener(TransferHistoryAdapter.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
