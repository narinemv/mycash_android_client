package com.mycash.client.ui.view;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.model.entity.TransferBonus;

import java.security.SecureRandom;

/**
 * Created by Narine on 02,July,2018
 */
public interface ITransferBonusView {

    void showProgress();

    void hideProgress();

    void setBranchNumberError();

    void setBonusError();

    void setBonusError(MyCashClientConstants.TRANSFER_ERROR_TYPE error);

    void setPhoneError();

    void navigateToFinishPage(TransferBonus transferBonus);

    void setTransferResponseError(String responseError);

    void phoneValidationSuccess(CheckOtherBuyerStateResponse phoneResponse);

    void setPhoneValidationResponseError (String responseError);

    void branchNumberValidationSuccess (BuyerBonusResponse branchResponse);

    void setBranchValidationResponseError (String responseError);

    void showNoConnectionMessage();
}
