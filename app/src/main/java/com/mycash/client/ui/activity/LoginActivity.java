package com.mycash.client.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.LocaleManager;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.LoginInteractorImpl;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.presenter.ILoginPresenter;
import com.mycash.client.presenter.LoginPresenterImpl;
import com.mycash.client.ui.view.ILoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class LoginActivity extends BaseActivity implements ILoginView {

    @BindView(R.id.usernameEditText)
    EditText usernameEditText;

    @BindView(R.id.pswdEditText)
    EditText pswdEditText;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    @BindView(R.id.loginButton)
    Button loginButton;

    @BindView(R.id.registerButton)
    Button registerButton;

    @BindView(R.id.forgotPassword)
    TextView forgotPassword;

    @BindView(R.id.phoneTLn)
    TextInputLayout phoneTLn;

    @BindView(R.id.pswdTLn)
    TextInputLayout pswdTLn;

    private String ccc;
    private ILoginPresenter presenter;
    private String code;

    @Override
    protected void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        presenter = new LoginPresenterImpl(this, MyCashClientApplication.getNetworkService(), new LoginInteractorImpl());
        ccc = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                ccc = ccp.getSelectedCountryCodeWithPlus();
                code = ccp.getSelectedCountryNameCode().toLowerCase();
                changeAppLanguage(LocaleManager.convertCodeCountryToLanguage(code));
            }
        });

        setupAppLanguage();
    }

    private void setupAppLanguage(){
        if (!LocaleManager.getLanguage(this).isEmpty()){
            code = LocaleManager.getLanguage(this);
            if (LocaleManager.isCountryCodeHasSupportedLanguage(ccp.getSelectedCountryNameCode().toLowerCase())){
                code = LocaleManager.convertCodeCountryToLanguage(ccp.getSelectedCountryNameCode().toLowerCase());
            }
         }else {
            code = ccp.getSelectedCountryNameCode().toLowerCase();
        }
        changeAppLanguage(code);
    }

    private void changeAppLanguage(String code){
        LocaleManager.setNewLocale(this, code);
        updateLoginScreenUIText();
    }

    private void updateLoginScreenUIText() {
        phoneTLn.setHint(getString(R.string.phone));
        pswdTLn.setHint(getString(R.string.password));
        loginButton.setText(R.string.login);
        registerButton.setText(R.string.register);
        forgotPassword.setText(R.string.forgot_password);
    }

    @Optional
    @OnClick(R.id.loginButton)
    public void onButtonClick () {
        // 1221_1575
        // bRY

        // +37496232323
        // 123456
        if (ConnectionHelper.isConnected(getApplicationContext())){
            presenter.validateCredentials(ccc, usernameEditText.getText().toString(), pswdEditText.getText().toString());
        }else{
            showNoConnectionMessage();
        }
    }

    @Optional
    @OnClick(R.id.registerButton)
    public void onRegButtonClick (){
        goToActivity(RegisterActivity.class);
    }

    @Optional
    @OnClick(R.id.forgotPassLn)
    public void onForgotPassClick (){
        goToActivity(ForgotPasswordActivity.class);
    }

    @Override
    public void showProgress() {
        _showProgressBar();
    }

    @Override
    public void hideProgress() {
        _hideProgressBar();
    }

    @Override
    public void setFieldsEmpty() {
        usernameEditText.setError(getResources().getString(R.string.msg_username_empty));
        pswdEditText.setError(getResources().getString(R.string.msg_password_empty));
    }

    @Override
    public void setUsernameError() {
        if(usernameEditText != null)
           usernameEditText.setError(getResources().getString(R.string.msg_username_empty));
    }

    @Override
    public void setPasswordError() {
        if(pswdEditText != null)
           pswdEditText.setError(getResources().getString(R.string.msg_password_empty));
    }

    @Override
    public void navigateToHome(LoginResponse loginResponse) {
        Utils.saveData(loginResponse, this);

        MyCashClientApplication.getNetworkService().updateToken(loginResponse.getToken());

        goToActivity(NavigationActivity2.class);
        finish();
    }

    @Override
    public void setResponseError(String responseError) {
        _showError(responseError);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(this, getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if(presenter != null)
           presenter.onDestroy();

        super.onDestroy();
    }
}