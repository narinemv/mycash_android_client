package com.mycash.client.ui.view;

import com.mycash.client.model.entity.PaymentHistoryDetails;
import com.mycash.client.model.entity.TransferHistoryDetails;

/**
 * Created by Narine on 01,June,2018
 */
public interface ITransferHistoryDetailsView {

    void showProgress();

    void hideProgress();

    void showTransferHistoryDetails(TransferHistoryDetails transferDetails);

    void showTransferHistoryDetailsResponseError(String error);

    void showNoConnectionMessage();
}
