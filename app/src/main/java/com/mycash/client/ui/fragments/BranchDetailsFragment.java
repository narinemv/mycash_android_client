package com.mycash.client.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.Request;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.activity.ViewPagerActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class BranchDetailsFragment extends Fragment implements
        BaseSliderView.OnSliderClickListener {

    @BindView(R.id.slider)
    SliderLayout imageSlider;

//  @BindView(R.id.custom_indicator)
//  PagerIndicator pagerIndicator;

    @BindView(R.id.branchDetailsTotalBonusTextView)
    TextView branchDetailsTotalBonusTextView;

    @BindView(R.id.branchDetailsImageView)
    ImageView branchDetailsImageView;

//  @BindView(R.id.detailsDiscountTextView)
//  TextView detailsDiscountTextView;

    @BindView(R.id.detailsCashBackTextView)
    TextView detailsCashBackTextView;

//  @BindView(R.id.detailsBonusTextView)
//  TextView detailsBonusTextView;

    @BindView(R.id.detailsDescEditText)
    TextView detailsDescEditText;

    @BindView(R.id.storeTimeTextView)
    TextView storeTimeTextView;

    @BindView(R.id.storePhoneLn)
    LinearLayout storePhoneLn;

    @BindView(R.id.webSiteLn)
    LinearLayout webSiteLn;

    @BindView(R.id.detailsSliderImage)
    ImageView detailsSliderImage;

    private Unbinder unbinder;
    private ArrayList<String> imageUrlsTmbList;
    private List<BranchResponse> branchResponseList;
    private BranchResponse selectedBranchInfo;
    private CompositeDisposable compositeDisposable;
    private int selectedBranchNum = 0;
    private String phoneNumber;
    private String viberNumber;
    private int bonus = 0;

    public BranchDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            branchResponseList = (List<BranchResponse>) getArguments().getSerializable(MyCashClientConstants.BRANCHES_LIST);
            selectedBranchNum = getArguments().getInt(MyCashClientConstants.SELECTED_BRANCH_NUM);
        }

        ((BaseActivity)getActivity()).setupActionBar("", true, false);

        selectedBranchInfo = branchResponseList.get(selectedBranchNum);
        if( selectedBranchInfo.getImageUrl() != null && !selectedBranchInfo.getImageUrl().isEmpty()) {
            imageUrlsTmbList = new ArrayList(Arrays.asList(selectedBranchInfo.getImageUrl().split(",")));
        }
        phoneNumber = selectedBranchInfo.getCCC() + selectedBranchInfo.getPhone();
        viberNumber = selectedBranchInfo.getEPhone();
        //Utils.getPhoneNumber(getContext());
    }

    private void handleResponse(BuyerBonusResponse buyerBonusResponse) {
        bonus = buyerBonusResponse.getBonus();
        branchDetailsTotalBonusTextView.setText(String.valueOf(bonus) + " " + getString(R.string.currency));
    }

    private void handleError(Throwable  error){
        ((BaseActivity)getActivity())._hideProgressBar();
        ((BaseActivity)getActivity())._showError(MyCashClientApplication.getNetworkService().getErrorMessage(error));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_branch_details, container, false);

        unbinder = ButterKnife.bind(this, view);

        initSlider();

        loadImage();

        setupUIData();

        return view;
    }

    private void setupUIData() {
        branchDetailsTotalBonusTextView.setText(String.valueOf(bonus));
        //detailsDiscountTextView.setText(getActivity().getResources().getString(R.string.sale) + " " + String.valueOf(selectedBranchInfo.getDiscount()) + "%");
        detailsCashBackTextView.setText(getString(R.string.bonus) + " " +String.valueOf(selectedBranchInfo.getBonus1()) + "%");
        //detailsBonusTextView.setText(getActivity().getResources().getString(R.string.bonus) + " " + String.valueOf(selectedBranchInfo.getBonus1()) + "%");
        detailsDescEditText.setText(selectedBranchInfo.getAboutUs());

        String storeStartTime = "00:00", storeEndTime = "00:00";

        if (selectedBranchInfo.getWorkStart() != null){ //&& selectedBranchInfo.getWorkEnd() != null) {
            storeStartTime = getFormatedTime(selectedBranchInfo.getWorkStart());
        }
        if (selectedBranchInfo.getWorkEnd() != null){
            storeEndTime = getFormatedTime(selectedBranchInfo.getWorkEnd());
        }

        storeTimeTextView.setText(storeStartTime + " - " + storeEndTime);
    }

    private String getFormatedTime (String time){
        return time.substring(0, 5);
    }

    private void loadImage (){
        Picasso.with(getContext()).load(MyCashClientApplication.getNetworkService().getBaseUrl() + MyCashClientConstants.LOGO_API + selectedBranchInfo.getUserId())
                .resize(120,100)
                .into(branchDetailsImageView);
    }

    private void initSlider() {
        DefaultSliderView defaultSliderView = null;

        if (imageUrlsTmbList != null && imageUrlsTmbList.size() > 0) {
            imageSlider.setVisibility(View.VISIBLE);
            detailsSliderImage.setVisibility(View.GONE);

            for (String name : imageUrlsTmbList) {

                defaultSliderView = new DefaultSliderView(getActivity());
                defaultSliderView
                        .image(Utils.getImagesBaseUrl(getContext()) + name)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(this);
                //imageSlider.setCustomIndicator(pagerIndicator);
                imageSlider.addSlider(defaultSliderView);
            }
            if (imageUrlsTmbList.size() == 1){
                imageSlider.stopAutoCycle();
            }
        }else{
/*
            defaultSliderView
                    .image(R.drawable.green_logo)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            imageSlider.stopAutoCycle();
*/
            imageSlider.setVisibility(View.GONE);
            detailsSliderImage.setVisibility(View.VISIBLE);
        }

        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
        super.onDestroyView();
    }

    @OnClick(R.id.webSiteLn)
    public void openWebSite (){
        String url = MyCashClientApplication.getNetworkService().getBaseUrl() + MyCashClientConstants.WEB_SITE_API + selectedBranchInfo.getUserId();
        Utils.openWebURL(url, getContext());
    }

    @OnClick(R.id.mapLn)
    public void openMap (){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putBoolean(MyCashClientConstants.OPEN_MAP_FROM_CATEGORY, true);
        args.putSerializable(MyCashClientConstants.SELECTED_BRANCH, (Serializable) selectedBranchInfo);

        Fragment fragment = new MapFragment();
        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.add(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @OnClick(R.id.videoURL)
    public void openVideoUrl (){
        String videoUrl = selectedBranchInfo.getVideoUrl();
        Utils.openWebURL(videoUrl, getContext());
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Intent intent = new Intent(getActivity(), ViewPagerActivity.class);
        intent.putStringArrayListExtra(MyCashClientConstants.IMAGE_URLS, imageUrlsTmbList);
        startActivity(intent);
    }

    @OnClick(R.id.payFromDetailsBtn)
    public void pay (){
        goToPayInCashFragment(new PayInCashFragment(), String.valueOf(selectedBranchInfo.getId()));
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.storePhoneLn)
    public void doPhoneCall (View view) {
        PopupMenu popup = new PopupMenu(getActivity(), storePhoneLn);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.phone_popup, popup.getMenu());
        popup.getMenu().getItem(0).setTitle(phoneNumber);
        popup.getMenu().getItem(1).setTitle(viberNumber);

        MenuPopupHelper menuHelper = new MenuPopupHelper(getActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch(id) {
                    case R.id.phone:
                        doPhoneCall();
                        return true;
                    case R.id.viber:
                        doViberCall();
                        return true;
                   /* case R.id.whatsapp:
                        //doWhatsAppCall();
                        return true;*/
                    default:
                        return false;
                }
            }
        });
    }

    private void doPhoneCall () {
//        Uri call = Uri.parse("tel:" + phoneNumber);
//        Intent surf = new Intent(Intent.ACTION_DIAL, call);
//        startActivity(surf);
    }

    private void doViberCall () {
//        try {
//            Uri uri = Uri.parse("tel:" + Uri.encode(phoneNumber));
//            Intent intent = new Intent("android.intent.action.VIEW");
//            intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
//            intent.setData(uri);
//            startActivity(intent);
//        }catch (Exception e){
//            Log.e(MyCashClientConstants.TAG, "ViberCall Exaptation" + e.toString());
//            Toast.makeText(getContext(), getResources().getString(R.string.viber_error), Toast.LENGTH_LONG).show();
//        }
    }

    /*private void doWhatsAppCall () {
        try {
            Uri uri = Uri.parse("callto:" + "+37491951288");
            Intent i = new Intent(Intent.ACTION_CALL, uri);
            i.setPackage("com.whatsapp");
            startActivity(i);
        } catch (Exception e) {
            Log.e(MyCashClientConstants.TAG, "WhatsAppCall Exaptation" + e.toString());
        }
    }*/

    private void goToPayInCashFragment (Fragment fragment, String storeId) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putInt(MyCashClientConstants.BONUS, bonus);
        args.putString(MyCashClientConstants.STORE_ID, storeId);

        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar( "", true, false);
        getUserBonusForBranch();
    }

    private void getUserBonusForBranch (){
        Request request = new Request();
        request.setValue(String.valueOf(selectedBranchInfo.getId()));
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(MyCashClientApplication.getNetworkService().getAPI().getBranchResponse(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(buyerBonusResponses -> handleResponse(buyerBonusResponses), error -> handleError(error)));

    }
}
