package com.mycash.client.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.LocaleManager;
import com.mycash.client.model.entity.Category;
import com.mycash.client.model.entity.SubCategory;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.adapter.CategoryAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends Fragment implements CategoryAdapter.ItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Unbinder unbinder;

    @BindView(R.id.categoryRecyclerView)
    RecyclerView categoryRecyclerView;

    private List<Category> categoryList;
    private Category category;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        String jsonString = loadJSONFromAsset();
        Category[] category = new Gson().fromJson(jsonString, Category[].class);
        categoryList = Arrays.asList(category);

        final CategoryAdapter categoryAdapter = new CategoryAdapter(getActivity(), categoryList);
        int numberOfColumns = ((BaseActivity)getActivity()).getColNumDisplay();
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        categoryAdapter.setClickListener(this);
        categoryRecyclerView.setAdapter(categoryAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        unbinder = ButterKnife.bind(this, view);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.category), false, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDestroyView() {
        if(unbinder != null)
           unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(View view, int position) {
        category = categoryList.get(position);
        if(categoryList.get(position).getSubCategory() != null && categoryList.get(position).getSubCategory().size() > 0)
           goToSubCategoryList(new SubCategoryListFragment(), categoryList.get(position).getSubCategory());
    }

    private void goToSubCategoryList(Fragment fragment, List<SubCategory> subCategoryList) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putSerializable(MyCashClientConstants.SUB_CATEGORY_LIST, (Serializable) subCategoryList);
        args.putString(MyCashClientConstants.CATEGORY_IMAGE, category.getImage());
        args.putString(MyCashClientConstants.CATEGORY_NAME, category.getName());
        args.putString(MyCashClientConstants.CATEGORY_COLOR, category.getColor());

        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String loadJSONFromAsset() {
        String json = null;
        String jsonName = "category_eng.json";
        if (LocaleManager.getLanguage(getContext()).equals("hy")){
            jsonName = "category.json";
        }else if (LocaleManager.getLanguage(getContext()).equals("ru")){
            jsonName = "category_ru.json";
        }
        try {
            InputStream is = getContext().getAssets().open(jsonName);
            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            Log.i("", ex.toString());
            return null;
        }
        return json;
    }

}
