package com.mycash.client.ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbond.loadingbutton.LoadingButton;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.PayInCashInteractorImpl;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.presenter.IPayInCashPresenter;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.view.IPayInCashView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.internal.Util;

public class PayInCashFragment extends Fragment implements IPayInCashView{

    @BindView(R.id.branchEditText)
    TextInputEditText branchEditText;

    @BindView(R.id.payCashTextView)
    TextView payCashTextView;

    @BindView(R.id.bonusSwitch)
    Switch bonusSwitch;

    @BindView(R.id.amountEditText)
    TextInputEditText amountEditText;

    @BindView(R.id.payInCashScreenScrollView)
    ScrollView payInCashScreenScrollView;

    @BindView(R.id.accumulatedMoneyTextView)
    TextView accumulatedMoneyTextView;

    @BindView(R.id.branchTextInputLayout)
    TextInputLayout branchTextInputLayout;

    @BindView(R.id.checkBtn)
    LoadingButton checkBtn;

    private IPayInCashPresenter presenter;
    private Handler handler = new Handler();
    private long LAST_TEXT_EDIT = 0;
    private BuyerBonusResponse buyerBonusResponse;
    private boolean isBonusSwitched = false;
    private Unbinder unbinder;
    private String storeId;
    private String cashStr = "0";
    private int bonus = 0;

    public PayInCashFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new PayInCashInteractorImpl(this, MyCashClientApplication.getNetworkService());

        if (getArguments() != null) {
            storeId = getArguments().getString(MyCashClientConstants.STORE_ID);
            bonus = getArguments().getInt(MyCashClientConstants.BONUS);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pay_in_cash, container, false);

        unbinder = ButterKnife.bind(this, view);

        ((BaseActivity)getActivity()).setupActionBar(getString(R.string.pay_in_cash), true, false);

        setupListeners();

        if (storeId != null && !storeId.isEmpty()){
            branchEditText.setText(storeId);
            checkBtn.setVisibility(View.GONE);
        }

        accumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, String.valueOf(bonus)));
        payCashTextView.setText(getString(R.string.cash_amount, cashStr));

        return view;
    }

    private void setupListeners() {
        branchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                checkBtn.hideLoading();
            }
        });

        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                calculateCash();
            }
        });

        bonusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if( isChecked){
                    isBonusSwitched = true;

                    //payCashTextView.setText(getString(R.string.cash_amount, cashStr));
                }else{
                    isBonusSwitched = false;
                    //cashStr = amountEditText.getText().toString();
                    //payCashTextView.setText(getString(R.string.cash_amount, cashStr));
                }

                calculateCash();
            }
        });
    }

    @OnClick(R.id.checkBtn)
    public void check(){
        if (TextUtils.isEmpty(branchEditText.getText())){
            branchEditText.setError(getString(R.string.msg_branch_empty));
            return;
        }
        if (ConnectionHelper.isConnected(getContext())) {
            if( branchEditText != null) {
                presenter.loadRxData(branchEditText.getText().toString());
                checkBtn.hideLoading();
            }
        } else {
            showNoConnectionMessage();
        }
    }

    @OnClick(R.id.payCashBtn)
    public void showQRImage (){
        if (TextUtils.isEmpty(amountEditText.getText().toString())) {
            amountEditText.setError(getResources().getString(R.string.msg_amount_empty));
        }else{
            presenter.generateQRImage(getQRContentStr());
        }
    }

    private String getQRContentStr () {
        int amount = 0, buyerId = 0, cash = 0, bonus = 0;
        try {
            amount = Integer.valueOf(amountEditText.getText().toString());
            cash = Integer.valueOf(cashStr);
            if (buyerBonusResponse != null){
                bonus = buyerBonusResponse.getBonus();
                buyerId = buyerBonusResponse.getBuyerId();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        String generatedQRStr = Utils.generateQRContentStr(amount, bonus, buyerId, cash, isBonusSwitched);
        String encryptedQRStr = Utils.encryptedString(generatedQRStr);
        return encryptedQRStr;
    }

    private void calculateCash () {
        String amount = amountEditText.getText().toString();
        if( !TextUtils.isEmpty(amount)) {
            presenter.calculateCash(Integer.valueOf(amount), bonus, isBonusSwitched);
        }
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null ){
            unbinder.unbind();
        }
        if (presenter != null)
            presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showBuyerBonusResponseResults(BuyerBonusResponse response) {
        checkBtn.showSuccess();
        buyerBonusResponse = response;

        bonus = buyerBonusResponse.getBonus();
        branchTextInputLayout.setHint(getString(R.string.pay_branch_hint, buyerBonusResponse.getStore().getName()));
        accumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, String.valueOf(bonus)));

        if(bonus > 0) {
            bonusSwitch.setEnabled(true);
        }

        calculateCash();
    }

    @Override
    public void showBuyerBonusResponseFailure(String error) {
        checkBtn.showError();
        bonusSwitch.setEnabled(false);
        accumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, "0"));
        branchTextInputLayout.setHint(getString(R.string.enter_branch_number));

        buyerBonusResponse = null;

        ((BaseActivity)getActivity())._showError(error);
    }

    @Override
    public void showCash(String cash) {
        payCashTextView.setText(getString(R.string.cash_amount, cash));
        cashStr = cash;
    }

    @Override
    public void passQRImage(Bitmap bitmap) {
        if( bitmap != null ){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            goToFragmentWithByteArray(new ShowQRFragment(), stream.toByteArray());
        }
    }

    private void goToFragmentWithByteArray(Fragment fragment, byte[] byteArray) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putByteArray(MyCashClientConstants.QR_CONTENT, byteArray);

        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.add(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
