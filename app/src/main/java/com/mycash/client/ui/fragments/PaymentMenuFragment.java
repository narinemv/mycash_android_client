package com.mycash.client.ui.fragments;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.AdvertisementResponse;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.ui.activity.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PaymentMenuFragment extends Fragment implements BaseSliderView.OnSliderClickListener  {

    @BindColor(R.color.colorPrimary)
    int colorPrimary;

    @BindColor(R.color.colorMenuBtn2)
    int colorMenuBtn2;

    @BindColor(R.color.colorMenuBtn3)
    int colorMenuBtn3;

    @BindColor(R.color.colorMenuBtn4)
    int colorMenuBtn4;

    @BindView(R.id.payInCashBtn)
    Button payInCashBtn;

    @BindView(R.id.transferableBonusBtn)
    Button transferableBonusBtn;

    @BindView(R.id.sliderImage)
    ImageView sliderImage;
/*
    @BindView(R.id.menu3Btn)
    Button menu3Btn;*/

    @BindView(R.id.menu4Btn)
    Button menu4Btn;

    @BindView(R.id.menuSlider)
    SliderLayout menuSlider;

//    @BindView(R.id.menu_slider_indicator)
//    PagerIndicator menuSliderIndicator;

    private Unbinder unbinder;
    private List<AdvertisementResponse> advertisementList;
    private List<BranchResponse> branchResponseList;
    private CompositeDisposable compositeDisposable;

    public PaymentMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //context = getActivity().getApplicationContext();
        //service = MyCashClientApplication.getNetworkService();
        compositeDisposable = new CompositeDisposable();
    }

    private void setupButtonsColor() {
        //change color of menu first button
        ((GradientDrawable) payInCashBtn.getBackground()).setColor(colorPrimary);

        //change color of menu second button
        ((GradientDrawable) menu4Btn.getBackground()).setColor(colorMenuBtn2);

        //change color of menu second button
        ((GradientDrawable) transferableBonusBtn.getBackground()).setColor(colorMenuBtn4);
/*
        //change color of menu second button
        ((GradientDrawable) menu3Btn.getBackground()).setColor(colorMenuBtn3);*/

        //change color of menu first button
        ((GradientDrawable) payInCashBtn.getBackground()).setColor(colorPrimary);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void handleResponse(List<AdvertisementResponse> response) {
        advertisementList = response;
        if (advertisementList.size() > 0) {
            initSlider(advertisementList);
        }
    }

    private void handleError(Throwable  error){
       // ((BaseActivity)getActivity())._showError(MyCashClientApplication.getNetworkService().getErrorMessage(error));
        //Log.e(MyCashClientConstants.TAG, MyCashClientApplication.getNetworkService().getErrorMessage(error));
    }

    private void initSlider(List<AdvertisementResponse> advertisementList) {

        menuSlider.setVisibility(View.VISIBLE);
        sliderImage.setVisibility(View.GONE);
        branchResponseList = new ArrayList<>();
        DefaultSliderView defaultSliderView = null;

        if (advertisementList.size() > 0) {
            for (int i = 0; i < advertisementList.size(); i++) {
                 defaultSliderView = new DefaultSliderView(getActivity());
                 AdvertisementResponse response = advertisementList.get(i);
                 branchResponseList.add(response.getStore());

                 defaultSliderView
                        .image(Utils.getImagesBaseUrl(getContext()) + response.getAdvertisement().getImgUrl())
                         //.description(response.getAdvertisement().getTxt())
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);

                 menuSlider.addSlider(defaultSliderView);
                 //menuSlider.setCustomIndicator(menuSliderIndicator);
            }
            if (advertisementList.size() == 1){
                menuSlider.stopAutoCycle();
            }
        } else{
/*            defaultSliderView
                    .image(R.drawable.green_logo)
                    .setScaleType(BaseSliderView.ScaleType.CenterInside);
            menuSlider.stopAutoCycle();*/
            menuSlider.setVisibility(View.GONE);
            sliderImage.setVisibility(View.VISIBLE);
        }

        menuSlider.setPresetTransformer(SliderLayout.Transformer.DepthPage);
    }

    TimerTask timerTask = null;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((BaseActivity)getActivity()).setupActionBar("", false, false);

        setupButtonsColor();

        Timer timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (Utils.getLongitude(getContext()) > -1 && Utils.getLatitude(getContext()) > -1){
                    timerTask.cancel();
                    compositeDisposable.add(MyCashClientApplication.getNetworkService().getAPI().getAdvertisements(((BaseActivity)getActivity()).createLocationRequest())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(response -> handleResponse(response), error -> handleError(error)));
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 500, 500);
    }

    @OnClick(R.id.payInCashBtn)
    public void onPayInCashBtnClick () {
        ((BaseActivity)getActivity()).goToFragment(new PayInCashFragment());
    }

    @OnClick(R.id.transferableBonusBtn)
    public void onTransferBonusBtnClick (){
        ((BaseActivity)getActivity()).goToFragment(new TransferBonusFragment());
    }

    @Override
    public void onDestroyView() {
        if(unbinder != null)
           unbinder.unbind();
        if (compositeDisposable != null)
            //compositeDisposable.dispose();
        if (advertisementList != null) {
            //advertisementList.clear();
            //advertisementList = null;
        }
        super.onDestroyView();
    }

     @Override
    public void onSliderClick(BaseSliderView slider) {
        int position = getPosition(slider.getUrl());
        ((BaseActivity)getActivity()).goToBranchDetailsFragment(new BranchDetailsFragment(), branchResponseList, position);
    }

    private int getPosition (String url){
        for(int i = 0; i < advertisementList.size(); i++){
            String imageUrl = Utils.getImagesBaseUrl(getContext()) + advertisementList.get(i).getAdvertisement().getImgUrl();
            if (imageUrl.equalsIgnoreCase(url)){
                return i;
            }
        }
        return 0;
    }
}
