package com.mycash.client.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.ui.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ShowMessageFragment extends Fragment {

    @BindView(R.id.tranferResultTextView)
    TextView tranferResultTextView;

    private Unbinder unbinder;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TRANSFER_FINISH_RESULT = "transfer_finish_result";

    // TODO: Rename and change types of parameters
    private String param;

    public ShowMessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param Parameter 1.
     * @return A new instance of fragment ShowMessageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowMessageFragment newInstance(String param) {
        ShowMessageFragment fragment = new ShowMessageFragment();
        Bundle args = new Bundle();
        args.putString(TRANSFER_FINISH_RESULT, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            param = getArguments().getString(TRANSFER_FINISH_RESULT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_message, container, false);

        unbinder = ButterKnife.bind(this, view);

        tranferResultTextView.setText(param);

        ((BaseActivity)getActivity()).setupActionBar( "", false, true);

        return view;
    }

    @Override
    public void onDestroyView() {
        if(unbinder != null)
           unbinder.unbind();
        super.onDestroyView();
    }
}
