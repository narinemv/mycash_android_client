package com.mycash.client.ui.view;

import com.mycash.client.model.entity.LoginResponse;

public interface ILoginView {

    void showProgress();

    void hideProgress();

    void setFieldsEmpty();

    void setUsernameError();

    void setPasswordError();

    void navigateToHome(LoginResponse loginResponse);

    void setResponseError(String responseError);

    void showNoConnectionMessage();
}