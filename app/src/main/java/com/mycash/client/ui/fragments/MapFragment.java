package com.mycash.client.ui.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.api.NetworkService;
import com.mycash.client.model.entity.BranchResponse;
import com.mycash.client.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener {

    private GoogleMap map;
    private CompositeDisposable compositeDisposable;
    private boolean isFromCategory = false;
    private List<BranchResponse> branchList = new ArrayList<BranchResponse>();

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        ((BaseActivity) getActivity()).setupActionBar(getString(R.string.map), false, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (getArguments() != null && getArguments().containsKey(MyCashClientConstants.OPEN_MAP_FROM_CATEGORY)){
            isFromCategory = getArguments().getBoolean(MyCashClientConstants.OPEN_MAP_FROM_CATEGORY, false);
        }
        if (getArguments() != null && getArguments().containsKey(MyCashClientConstants.SELECTED_BRANCH)) {
            branchList.add((BranchResponse) getArguments().getSerializable(MyCashClientConstants.SELECTED_BRANCH));
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFromCategory){
            ((BaseActivity) getActivity()).setupActionBar(getString(R.string.map), true, false);
        }
    }

    public void getBranches() {
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(MyCashClientApplication.getNetworkService().getAPI().getBranches()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(buyerBonusResponses -> handleResponse(buyerBonusResponses), error -> handleError(error)));
    }

    private void handleResponse(List<BranchResponse> branchResponse) {
        if (branchResponse.size() > 0) {
            showMarkers(branchResponse);
        }
    }

    private void showMarkers(List<BranchResponse> branchList){
        if (branchList == null || branchList.size() < 1){
            return;
        }
        for (int i = 0; i < branchList.size(); i++) {
            BranchResponse branch = branchList.get(i);
            if (branch.getLatitude() != null && branch.getLongitude() != null) {
                Log.i("Tag", "::33333333 = " + i);
                double offsetLat = branch.getLatitude();
                double offsetLong = branch.getLongitude();

                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(offsetLat, offsetLong))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                        .title(branch.getName())
                        .snippet(branch.getAddress()));
                marker.setTag(i);

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        // Getting view from the layout file
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.marker_title, null);

                        TextView title = view.findViewById(R.id.marker_branch_name);
                        title.setText(marker.getTitle());

                        TextView address = view.findViewById(R.id.marker_branch_address);
                        address.setText(marker.getSnippet());

                        setupKMTextView(view, branch);

                        return view;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        return null;
                    }
                });
                if(isFromCategory) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(offsetLat, offsetLong), 16));
                }

                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        //Toast.makeText(getActivity(), "Marker Title clicked", Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).goToBranchDetailsFragment(new BranchDetailsFragment(), branchList, (int) marker.getTag());
                    }
                });
            }
        }
    }

    private void setupKMTextView (View view, BranchResponse branchResponse){

        TextView distanceKm = view.findViewById(R.id.distanceKmTextView);

        Location branchLocation = new Location("");
        branchLocation.setLatitude(branchResponse.getLatitude());
        branchLocation.setLongitude(branchResponse.getLongitude());
        double distance = Math.round(branchLocation.distanceTo(location)/1000 *10.0)/10.0;

        distanceKm.setText(String.valueOf(distance) + " " + getString(R.string.km));
    }

    private void handleError(Throwable error) {
        ((BaseActivity) getActivity())._hideProgressBar();
        //((BaseActivity) getActivity())._showError(MyCashClientApplication.getNetworkService().getErrorMessage(error));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }

    @Override
    public void onDestroyView() {
        if (compositeDisposable != null)
            compositeDisposable.dispose();
        super.onDestroyView();
        if (isFromCategory){
            ((BaseActivity) getActivity()).setupActionBar("", true, false);
        }
    }

    Location location;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions

            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        location = ((BaseActivity)getActivity()).getLocation();
        if( location != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 14));
        }
        if (!isFromCategory) {
            getBranches();
        }else{
            showMarkers(branchList);
        }
    }
}
