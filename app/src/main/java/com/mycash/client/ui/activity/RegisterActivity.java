package com.mycash.client.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.RegistrationInteractorImpl;
import com.mycash.client.model.entity.LoginResponse;
import com.mycash.client.presenter.IRegistrationPresenter;
import com.mycash.client.presenter.RegistrationPresenterImpl;
import com.mycash.client.ui.view.IRegisterView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements IRegisterView{

    @BindView(R.id.reg_first_name)
    EditText regFirstNameEditText;

    @BindView(R.id.reg_last_name)
    EditText regLastNameEditText;

    @BindView(R.id.reg_phone)
    EditText regPhoneEditText;

    @BindView(R.id.reg_email)
    EditText regEmailEditText;

    @BindView(R.id.reg_pswd)
    EditText regPswdEditText;

    @BindView(R.id.reg_confirm_pswd)
    EditText regConfirmPswdEditText;

    @BindView(R.id.regCodeEditText)
    EditText regCodeEditText;

    @BindView(R.id.registrationLn)
    LinearLayout registrationLn;

    @BindView(R.id.regCodeValidationRL)
    LinearLayout regCodeValidationRL;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    private String ccc;
    private IRegistrationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

        setupActionBar(getString(R.string.register), false, true);

        presenter = new RegistrationPresenterImpl(this, MyCashClientApplication.getNetworkService(), new RegistrationInteractorImpl());
        ccc = ccp.getSelectedCountryCodeWithPlus();
        //ccp.registerCarrierNumberEditText(regPhoneEditText);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                 ccc = ccp.getSelectedCountryCodeWithPlus();
            }
        });
        //+48 9677777723
        //123456
    }

    @OnClick(R.id.registerButton)
    public void registration () {
        if (ConnectionHelper.isConnected(getApplicationContext())) {
            presenter.validateRegistrationCredentials(ccc, regFirstNameEditText.getText().toString(),
                    regLastNameEditText.getText().toString(),
                    regEmailEditText.getText().toString(),
                    regPhoneEditText.getText().toString(),
                    regPswdEditText.getText().toString(),
                    regConfirmPswdEditText.getText().toString());
        } else {
            showNoConnectionMessage();
        }
    }

    @Override
    public void showProgress() {
       _showProgressBar();
    }

    @Override
    public void hideProgress() {
       _hideProgressBar();
    }

    @Override
    public void setFirstNameEmpty() {
        regFirstNameEditText.setError(getString(R.string.msg_firstname_empty));
    }

    @Override
    public void setLastNameEmpty() {
        regLastNameEditText.setError(getString(R.string.msg_lastname_empty));
    }

    @Override
    public void setPhoneEmpty() {
        regPhoneEditText.setError(getString(R.string.msg_phone_empty));
    }

    @Override
    public void setEmailEmpty() {
        regEmailEditText.setError(getString(R.string.msg_email_empty));
    }

    @Override
    public void setPasswordEmpty() {
        regPswdEditText.setError(getString(R.string.msg_password_empty));
    }

    @Override
    public void setConfirmPasswordEmpty() {
        regConfirmPswdEditText.setError(getString(R.string.msg_confirm_pswd_empty));
    }

    @Override
    public void setFieldsEmpty() {
        regFirstNameEditText.setError(getString(R.string.msg_firstname_empty));
        regLastNameEditText.setError(getString(R.string.msg_lastname_empty));
        regPhoneEditText.setError(getString(R.string.msg_phone_empty));
        regEmailEditText.setError(getString(R.string.msg_email_empty));
        regPswdEditText.setError(getString(R.string.msg_password_empty));
        regConfirmPswdEditText.setError(getString(R.string.msg_confirm_pswd_empty));
    }

    @Override
    public void setEmailNonValid() {
        regEmailEditText.setError(getString(R.string.msg_email_error));
    }

    @Override
    public void setPswdNotConfirmed() {
        regPswdEditText.setError(getString(R.string.msg_password_error));
        regConfirmPswdEditText.setError(getString(R.string.msg_password_error));
    }

    @Override
    public void navigateToHome(LoginResponse response) {
        registrationLn.setVisibility(View.GONE);
        regCodeValidationRL.setVisibility(View.VISIBLE);
        setupActionBar("", true, false);
    }

    @Override
    public void setRegistrationResponseError(String responseError) {
       _showError(responseError);
    }

    @OnClick(R.id.regDoneButton)
    public void onDoneBtnClick () {
        if (ConnectionHelper.isConnected(getApplicationContext())) {
            presenter.validateCode(regCodeEditText.getText().toString());
        } else {
            showNoConnectionMessage();
        }
    }

    @Override
    public void setCodeEmpty() {
        regCodeEditText.setError(getString(R.string.msg_code_empty));
    }

    @Override
    public void goToLoginPage(LoginResponse response) {
        Utils.saveData(response, this);

        MyCashClientApplication.getNetworkService().updateToken(response.getToken());

        goToActivity(NavigationActivity2.class);
        finish();
    }

    @Override
    public void setValidationResponseError(String responseError) {
        _showError(responseError);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(this, getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if(presenter != null)
           presenter.onDestroy();

        super.onDestroy();
    }
}
