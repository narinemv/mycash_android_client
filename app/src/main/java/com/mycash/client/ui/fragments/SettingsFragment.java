package com.mycash.client.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.beautycoder.pflockscreen.security.PreferencesSettings;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.LocaleManager;
import com.mycash.client.helper.Utils;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.activity.LoginActivity;
import com.mycash.client.ui.activity.NavigationActivity2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mycash.client.helper.LocaleManager.LANGUAGE_ARMENIAN;
import static com.mycash.client.helper.LocaleManager.LANGUAGE_ENGLISH;
import static com.mycash.client.helper.LocaleManager.LANGUAGE_RUSSIAN;

public class SettingsFragment extends Fragment {

    private String[] menuItems;
    private Unbinder unbinder;
    static String DEFAULT_DISTANCE = "50";
    static String REG_COMPANY_URL = "https://www.mycash.am/pages/#/registration";
    static String ABOUT_URL = "https://www.mycash.am/pages/about.pdf";
    String[] languagesArray;

    @BindView(R.id.distanceEditText)
    EditText distanceEditText;

    @BindView(R.id.settingsUserTextView)
    TextView settingsUserTextView;

    @BindView(R.id.changeLanguageTextView)
    TextView changeLanguageTextView;

    @BindView(R.id.languageValueTextView)
    TextView languageValueTextView;

    @BindView(R.id.settingsDistanceTextView)
    TextView settingsDistanceTextView;

    @BindView(R.id.settingsKmTextView)
    TextView settingsKmTextView;

    @BindView(R.id.settingsAboutUsTextView)
    TextView settingsAboutUsTextView;

    @BindView(R.id.settingsRegCompanyTextView)
    TextView settingsRegCompanyTextView;

    @BindView(R.id.settingsChangePinTextView)
    TextView settingsChangePinTextView;

    @BindView(R.id.settingsSignOutTextView)
    TextView settingsSignOutTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        unbinder = ButterKnife.bind(this, view);

        languagesArray = getResources().getStringArray(R.array.language_list);

        String distance = Utils.getDistance(getContext());
        if( distance != null){
            distanceEditText.setText(distance);
        } else {
            distanceEditText.setText(DEFAULT_DISTANCE);
        }

        String userName = Utils.getLoggedInUserName(getActivity());
        if (userName != null) {
            settingsUserTextView.setText(userName);
        }

        String language = LocaleManager.getLanguage(getContext());
        if (!language.isEmpty()){
            if(language.equalsIgnoreCase("en")){
               languageValueTextView.setText(languagesArray[0]);
            }else if(language.equalsIgnoreCase("hy")){
                languageValueTextView.setText(languagesArray[1]);
            }else if(language.equalsIgnoreCase("ru")){
                languageValueTextView.setText(languagesArray[2]);
            }
        }else {
            languageValueTextView.setText(languagesArray[0]);
        }

        return view;
    }

    @OnClick(R.id.settingsChangePinLn)
    public void onChangePinClicked (){
        ((BaseActivity)getActivity()).createPinLockScreen(true);
    }

    @OnClick(R.id.settingsRegCompanyLn)
    public void onRegCompanyClicked (){
        Utils.openWebURL(REG_COMPANY_URL, getContext());
    }

    @OnClick(R.id.settingsUserLn)
    public void onUserProfileClicked (){
        ((BaseActivity)getActivity()).goToUserProfileFragment(new ProfileFragment());
    }

    @OnClick(R.id.settingsAboutUSLn)
    public void onAboutClicked (){
        Utils.openWebURL(ABOUT_URL, getContext());
    }

    @OnClick(R.id.settingsSignOutLn)
    public void onSignOutClicked (){
        Utils.removeToken(getContext());
        MyCashClientApplication.getNetworkService().removeToken();
        PreferencesSettings.removePin(getActivity().getApplicationContext());

        ((BaseActivity)getActivity()).goToActivity(LoginActivity.class);
        getActivity().finish();

        Utils.getSharedPreferences(getContext()).edit().clear().apply();
    }

    @OnClick(R.id.settingsChangeLanguageSLn)
    public void onChangeLanguageClicked () {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.change_language))
                .setItems(R.array.language_list, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {

                        if (languagesArray[position].equalsIgnoreCase("EN")) {
                            setNewLocale(LANGUAGE_ENGLISH, true);
                            languageValueTextView.setText(languagesArray[0]);
                        } else if (languagesArray[position].equalsIgnoreCase("RU")) {
                            setNewLocale(LANGUAGE_RUSSIAN, true);
                            languageValueTextView.setText(languagesArray[2]);
                        } else if (languagesArray[position].equalsIgnoreCase("AM")) {
                            setNewLocale(LANGUAGE_ARMENIAN, true);
                            languageValueTextView.setText(languagesArray[1]);
                        }
                    }
                }).create().show();
    }

    private boolean setNewLocale(String language, boolean restartProcess) {
        LocaleManager.setNewLocale(getActivity(), language);

        updateUITexts();

        return true;
    }

    private void updateUITexts() {
        //update ButtomNavigator texts after locale changed
        ((NavigationActivity2) getActivity()).navigation.getMenu().getItem(0).setTitle(R.string.pay);
        ((NavigationActivity2) getActivity()).navigation.getMenu().getItem(1).setTitle(R.string.category);
        ((NavigationActivity2) getActivity()).navigation.getMenu().getItem(2).setTitle(R.string.map);
        ((NavigationActivity2) getActivity()).navigation.getMenu().getItem(3).setTitle(R.string.history);
        ((NavigationActivity2) getActivity()).navigation.getMenu().getItem(4).setTitle(R.string.settings);

        changeLanguageTextView.setText(R.string.change_language);
        settingsDistanceTextView.setText(R.string.distance);
        settingsKmTextView.setText(R.string.km);
        settingsAboutUsTextView.setText(R.string.about_us);
        settingsRegCompanyTextView.setText(R.string.reg_company);
        settingsChangePinTextView.setText(R.string.change_pin);
        settingsSignOutTextView.setText(R.string.sign_out);
    }

    @Override
    public void onActivityCreated(Bundle savedBundle) {
        super.onActivityCreated(savedBundle);
        // We need to use a different list item layout for devices older than Honeycomb
        int layout = android.R.layout.simple_list_item_activated_1;

        distanceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                Utils.saveDistance(s.toString(), getContext());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.settings), false, false);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        super.onDestroyView();
    }
}