package com.mycash.client.ui.view;

import com.mycash.client.model.entity.PaymentHistoryDetails;

/**
 * Created by Narine on 01,June,2018
 */
public interface IHistoryDetailsView {

    void showProgress();

    void hideProgress();

    void showPaymentHistoryDetails(PaymentHistoryDetails details);

    void showPaymentHistoryDetailsResponseError (String error);

    void showNoConnectionMessage();
}
