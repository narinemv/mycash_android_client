package com.mycash.client.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.model.TransferHistoryPresenterImpl;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.presenter.ITransferHistoryPresenter;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.adapter.TransferHistoryAdapter;
import com.mycash.client.ui.view.ITransferHistoryView;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.mycash.client.ui.activity.BaseActivity.TRANSFER_HISTORY_ITEM;
import static com.mycash.client.ui.activity.BaseActivity.TRANSFER_ID;

public class TransferHistoryFragment extends Fragment implements ITransferHistoryView, TransferHistoryAdapter.ItemClickListener{

    @BindView(R.id.transferHistoryRecyclerview)
    RecyclerView transferHistoryRecyclerview;

    @BindView(R.id.transferDateHeraderTextView)
    TextView transferDateHeraderTextView;

    @BindView(R.id.transferBranchHeaderTextView)
    TextView transferBranchHeaderTextView;

    @BindView(R.id.transferBonusHeaderTextView)
    TextView transferBonusHeaderTextView;

    @BindView(R.id.transferHistoryNoDataTextxView)
    TextView transferHistoryNoDataTextxView;

    private Unbinder unbinder;
    private ITransferHistoryPresenter presenter;
    private List<TransferBonus> transfersList;

    public TransferHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new TransferHistoryPresenterImpl(this, MyCashClientApplication.getNetworkService());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transfer_history, container, false);

        unbinder = ButterKnife.bind(this, view);
        // Initialization of RecyclerView
        initRecyclerView();
        initUI();

       // ((NavigationActivity2)getActivity()).historyTabs.setVisibility(ViewPager.VISIBLE);

        if( transfersList== null)
            if ( ConnectionHelper.isConnected(getActivity())) {
                presenter.getTransferHistoryData();
            } else {
                showNoConnectionMessage();
            }
        return view;
    }

    private void initUI() {
        // As for fontFamily if you set a default font in Calligraphy then that will always override fontFamily
        // because of it as a solution here we are giving TextView style programmatically
        //transferDateHeraderTextView.setTypeface(transferDateHeraderTextView.getTypeface(), Typeface.BOLD);
        //transferBranchHeaderTextView.setTypeface(transferBranchHeaderTextView.getTypeface(), Typeface.BOLD);
        //transferBonusHeaderTextView.setTypeface(transferBonusHeaderTextView.getTypeface(), Typeface.BOLD);
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        transferHistoryRecyclerview.setLayoutManager(layoutManager);
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        if (presenter != null)
            presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void showTransferHistoryData(List<TransferBonus> transferList) {
        this.transfersList = transferList;

        transferHistoryRecyclerview.setVisibility(View.VISIBLE);
        transferHistoryNoDataTextxView.setVisibility(View.GONE);

        TransferHistoryAdapter transferHistoryAdapter = new TransferHistoryAdapter(getContext(), transferList);
        transferHistoryAdapter.setClickListener(this);
        transferHistoryRecyclerview.setAdapter(transferHistoryAdapter);
    }

    @Override
    public void showNoResult() {
        transferHistoryRecyclerview.setVisibility(View.GONE);
        transferHistoryNoDataTextxView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTransferHistoryResponseError(String error) {
        if( error.isEmpty()) {
            error = getResources().getString(R.string.msg_something_error);
        }
        //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(View view, int position) {
        Fragment fragment = new TransferHistoryDetailsFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putSerializable(TRANSFER_HISTORY_ITEM, transfersList.get(position));
        args.putString(TRANSFER_ID, String.valueOf(transfersList.get(position).getId()));
        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.add(R.id.history_fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
