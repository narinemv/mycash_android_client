package com.mycash.client.ui.view;

import com.mycash.client.model.entity.PaymentHistory;

import java.util.List;

/**
 * Created by Narine on 16,April,2018
 */
public interface IHistoryView {

    void showProgress();

    void hideProgress();

    void showPaymentHistoryData(List<PaymentHistory> paymentData);

    void showNoResult();

    void showPaymentHistoryResponseError (String error);

    void showNoConnectionMessage();
}