package com.mycash.client.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.ui.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.userNameTextView)
    TextView userNameTextView;

    @BindView(R.id.userSurNameTextView)
    TextView userSurNameTextView;

    @BindView(R.id.userPhoneTextView)
    TextView userPhoneTextView;

    @BindView(R.id.userEmailTextView)
    TextView userEmailTextView;

    private Unbinder unbinder;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        unbinder = ButterKnife.bind(this, view);

        if(userPhoneTextView != null) userPhoneTextView.setText(Utils.getPhoneNumber(getContext()));

        if(userNameTextView != null) userNameTextView.setText(Utils.getUserName(getContext()));

        if(userSurNameTextView != null) userSurNameTextView.setText(Utils.getUserLastName(getContext()));

        if(userEmailTextView != null) userEmailTextView.setText(Utils.getUserEmail(getContext()));

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.settings), true, false);
    }
}
