package com.mycash.client.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.model.entity.Category;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Narine on 12,March,2018
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context mContext;
    private List<Category> categoryList;
    private ItemClickListener clickListener;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public CategoryAdapter(Context context, List<Category> aReportList) {
        this.categoryList = aReportList;
        this.mContext = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_item, null);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final  CategoryViewHolder categoryViewHolder,  int position) {
        Category category = categoryList.get(position);

        categoryViewHolder.categoryTitle.setText(category.getName());
        categoryViewHolder.categoryTitle.setTextColor(Color.parseColor(category.getColor()));

        int drawableResourceId = mContext.getResources().getIdentifier(category.getImage(), "drawable", mContext.getPackageName());
        categoryViewHolder.categoryImage.setImageResource(drawableResourceId);
    }

    @Override
    public int getItemCount() {
        return categoryList == null ? 0 : categoryList.size();
    }

    protected class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.categoryTitle)
        TextView categoryTitle;

        @BindView(R.id.categoryImage)
        ImageView categoryImage;

        public CategoryViewHolder(View view) {
            super(view);

            // binding view
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public  void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}