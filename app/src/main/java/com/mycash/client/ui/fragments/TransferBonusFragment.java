package com.mycash.client.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbond.loadingbutton.LoadingButton;
import com.hbb20.CountryCodePicker;
import com.mycash.client.MyCashClientApplication;
import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.TransferBonusInteractorImpl;
import com.mycash.client.model.entity.BuyerBonusResponse;
import com.mycash.client.model.entity.CheckOtherBuyerStateResponse;
import com.mycash.client.model.entity.TransferBonus;
import com.mycash.client.presenter.ITransferBonusPresenter;
import com.mycash.client.presenter.TransferBonusPresneterImpl;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.view.ITransferBonusView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Unbinder;

public class TransferBonusFragment extends Fragment implements ITransferBonusView{

    @BindView(R.id.transferBranchEditText)
    TextInputEditText transferBranchEditText;

    @BindView(R.id.transferBranchTextInputLayout)
    TextInputLayout transferBranchTextInputLayout;

    @BindView(R.id.transferableBonusEditText)
    TextInputEditText transferableBonusEditText;

    @BindView(R.id.transferAccumulatedMoneyTextView)
    TextView transferAccumulatedMoneyTextView;

    @BindView(R.id.transfersPhoneEditText)
    EditText transfersPhoneEditText;

    @BindView(R.id.tranfersUserTextView)
    TextView tranfersUserTextView;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    @BindView(R.id.checkBranchBtn)
    LoadingButton checkBranchBtn;

    @BindView(R.id.checkPhoneBtn)
    LoadingButton checkPhoneBtn;

    private Unbinder unbinder;
    private Handler handler = new Handler();
    private Handler handler_1 = new Handler();
    private int bonus = 0;
    private String ccc;

    private ITransferBonusPresenter presenter;

    public TransferBonusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new TransferBonusPresneterImpl(this, MyCashClientApplication.getNetworkService(), new TransferBonusInteractorImpl());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupUIListeners();

        ((BaseActivity)getActivity()).setupActionBar(getString(R.string.transfer_bonus), true, false);

        transferAccumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, "0"));

        ccc = ccp.getSelectedCountryCodeWithPlus();
        //ccp.registerCarrierNumberEditText(usernameEditText);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                ccc = ccp.getSelectedCountryCodeWithPlus();
            }
        });

        return view;
    }

    private void setupUIListeners() {
        transferBranchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                checkBranchBtn.hideLoading();
            }
        });

        transfersPhoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                checkPhoneBtn.hideLoading();
            }
        });

        transfersPhoneEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if  (!hasFocus){
                    ((BaseActivity)getActivity()).closeKeyboard(transfersPhoneEditText);
                }
            }
        });
    }

    @OnClick(R.id.checkBranchBtn)
    public void setCheckBranch(){
        if (TextUtils.isEmpty(transferBranchEditText.getText())){
            transferBranchEditText.setError(getString(R.string.msg_branch_empty));
            return;
        }
        if (ConnectionHelper.isConnected(getContext())) {
            if( transferBranchEditText != null) {
                presenter.validateBranchNumber(transferBranchEditText.getText().toString());
            }
        } else {
            showNoConnectionMessage();
        }
    }

    @OnClick(R.id.checkPhoneBtn)
    public void setCheckPhone(){
        if (TextUtils.isEmpty(transfersPhoneEditText.getText())){
            transfersPhoneEditText.setError(getString(R.string.msg_phone_empty));
            return;
        }
        String phoneNumber = ccc + transfersPhoneEditText.getText().toString();
        String ownerPhoneNumber = Utils.getPhoneNumber(getContext());
        if (!ownerPhoneNumber.equals(phoneNumber)){
            if (ConnectionHelper.isConnected(getActivity())) {
                if( transfersPhoneEditText != null) {
                    presenter.validatePhone(phoneNumber);
                }
            } else {
                showNoConnectionMessage();
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.owner_bonus_error), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.transferBtn)
    public void doTransfer() {
        if (ConnectionHelper.isConnected(getActivity())) {
            presenter.validateCredentials(transferBranchEditText.getText().toString(),
                                          transferableBonusEditText.getText().toString(),
                                          String.valueOf(bonus),
                                          transfersPhoneEditText.getText().toString(),
                                          Utils.getPhoneNumber(getContext()));
        } else {
            showNoConnectionMessage();
        }
    }

    @Override
    public void onDestroyView() {
        if(unbinder != null){
           unbinder.unbind();
        }
        if(presenter != null)
           presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void setBranchNumberError() {
        transferBranchEditText.setError(getContext().getResources().getString(R.string.msg_branch_empty));
    }

    @Override
    public void setBonusError() {
        transferableBonusEditText.setError(getContext().getResources().getString(R.string.msg_bonus_empty));
    }

    @Override
    public void setBonusError(MyCashClientConstants.TRANSFER_ERROR_TYPE error) {
        switch (error){
            case NO_BONUS:
                transferBranchEditText.setError(getString(R.string.msg_transfer_no_bonus));
                break;
        }
    }

    @Override
    public void setPhoneError() {
        transfersPhoneEditText.setError(getContext().getResources().getString(R.string.msg_phone_empty));
    }

    @Override
    public void navigateToFinishPage(TransferBonus transferBonus) {
        String result = getString(R.string.msg_transfer_result, String.valueOf(transferBonus.getAmount()), transferBonus.getUserName());
        ShowMessageFragment fragment = ShowMessageFragment.newInstance(result);
        ((BaseActivity)getActivity()).goToFragment(fragment);
    }

    @Override
    public void setTransferResponseError(String responseError) {
        /*if( TextUtils.isEmpty(responseError)){
            responseError = getResources().getString(R.string.msg_something_error);
        }
        Toast.makeText(getActivity(), responseError, Toast.LENGTH_SHORT).show();*/
        ((BaseActivity)getActivity())._showError(responseError);
    }

    @Override
    public void phoneValidationSuccess(CheckOtherBuyerStateResponse phoneResponse) {
        checkPhoneBtn.showSuccess();
        tranfersUserTextView.setText(phoneResponse.getBuyerFirstName() + " " + phoneResponse.getBuyerLastName());
        ((BaseActivity)getActivity()).closeKeyboard(transfersPhoneEditText);
    }

    @Override
    public void setPhoneValidationResponseError(String responseError) {
        //transfersPhoneEditText.setText("");
        checkPhoneBtn.showError();
        tranfersUserTextView.setText("");
        ((BaseActivity)getActivity()).closeKeyboard(transfersPhoneEditText);
        ((BaseActivity)getActivity())._showError(responseError);
    }

    @Override
    public void branchNumberValidationSuccess(BuyerBonusResponse branchResponse) {
         checkBranchBtn.showSuccess();
         bonus = branchResponse.getBonus();
         transferBranchTextInputLayout.setHint(getString(R.string.pay_branch_hint, branchResponse.getStore().getName()));
         transferAccumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, String.valueOf(bonus)));

        ((BaseActivity)getActivity()).closeKeyboard(transfersPhoneEditText);
    }

    @Override
    public void setBranchValidationResponseError(String responseError) {
        checkBranchBtn.showError();
        transferAccumulatedMoneyTextView.setText(getString(R.string.pay_accumulated, "0"));
        transferBranchTextInputLayout.setHint(getString(R.string.enter_branch_number));

        ((BaseActivity)getActivity())._showError(responseError);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_SHORT).show();
    }
}
