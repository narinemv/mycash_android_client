package com.mycash.client.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.model.ForgotPasswordInteractorImpl;
import com.mycash.client.presenter.ForgotPasswordPresenterImpl;
import com.mycash.client.presenter.IForgotPasswordPresenter;
import com.mycash.client.ui.view.IForgotPasswordView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class ForgotPasswordActivity extends BaseActivity implements IForgotPasswordView{

    @BindView(R.id.forgot_pass_email)
    EditText forgotPassEmailEditText;

    @BindView(R.id.sendCodeRL)
    RelativeLayout sendCodeRL;

    @BindView(R.id.changePasswordRL)
    RelativeLayout changePasswordRL;

    @BindView(R.id.confirmPassword)
    EditText confirmPasswordEditText;

    @BindView(R.id.code)
    EditText codeEditText;

    @BindView(R.id.newPassword)
    EditText newPasswordEditText;

    private IForgotPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setupActionBar("", true, false);

        ButterKnife.bind(this);

        presenter = new ForgotPasswordPresenterImpl(this, MyCashClientApplication.getNetworkService(), new ForgotPasswordInteractorImpl());
    }

    @Optional
    @OnClick(R.id.sendCodeToEmailButton)
    public void onSendCodeToEmailButtonClick (){
        if (ConnectionHelper.isConnected(this)){
            presenter.validateEmail(forgotPassEmailEditText.getText().toString());
        }else{
            showNoConnectionMessage();
        }
    }

    @Optional
    @OnClick(R.id.doneButton)
    public void onDoneButtonClick (){
        if (ConnectionHelper.isConnected(this)){
            presenter.validateChangePasswordFields(codeEditText.getText().toString(),
                                                   newPasswordEditText.getText().toString(),
                                                   confirmPasswordEditText.getText().toString());
        }else{
            showNoConnectionMessage();
        }
    }

    @Override
    public void showProgress() {
        _showProgressBar();
    }

    @Override
    public void hideProgress() {
        _hideProgressBar();
    }

    @Override
    public void setEmailEmpty() {
        forgotPassEmailEditText.setError(getString(R.string.msg_email_empty));
    }

    @Override
    public void setEmailNonValid() {
        forgotPassEmailEditText.setError(getString(R.string.msg_email_error));
    }

    @Override
    public void setFieldsEmpty() {
        codeEditText.setError(getString(R.string.msg_code_empty));
        newPasswordEditText.setError(getString(R.string.msg_password_empty));
        confirmPasswordEditText.setError(getString(R.string.msg_confirm_pswd_empty));
    }

    @Override
    public void setCodeEmpty() {
        codeEditText.setError(getString(R.string.msg_code_empty));
    }

    @Override
    public void setNewPasswordEmpty() {
        newPasswordEditText.setError(getString(R.string.msg_password_empty));
    }

    @Override
    public void setConfirmPasswordEmpty() {
        confirmPasswordEditText.setError(getString(R.string.msg_confirm_pswd_empty));
    }

    @Override
    public void setPswdNotConfirmed() {
        newPasswordEditText.setError(getString(R.string.msg_password_error));
        confirmPasswordEditText.setError(getString(R.string.msg_password_error));
    }

    @Override
    public void showChangePassFields(String response) {
        sendCodeRL.setVisibility(View.GONE);
        changePasswordRL.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSendCodeToEmailError(String error) {
       _showError(error);
    }

    @Override
    public void finishChangePassword() {
        this.finish();
    }

    @Override
    public void showChangePasswordError(String error) {
        _showError(error);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(this, getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if (presenter != null)
            presenter.onDestroy();

        super.onDestroy();
    }
}
