package com.mycash.client.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.model.HistoryPresenterImpl;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.presenter.IHistoryPresenter;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.adapter.HistoryAdapter;
import com.mycash.client.ui.view.IHistoryView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.mycash.client.ui.activity.BaseActivity.HISTORY_ITEM;
import static com.mycash.client.ui.activity.BaseActivity.PAYMENT_ID;

public class PaymentHistoryFragment extends Fragment implements IHistoryView, HistoryAdapter.ItemClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView reposRecyclerView;

    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @BindView(R.id.cardTextView)
    TextView cardTextView;

    @BindView(R.id.cashTextView)
    TextView cashTextView;

    @BindView(R.id.fromBonusTextView)
    TextView fromBonusTextView;

    @BindView(R.id.no_data)
    TextView noDataView;

    private IHistoryPresenter presenter;
    private List<PaymentHistory> historyList;
    private Unbinder unbinder;
    private HistoryAdapter historyAdapter;
    private List<PaymentHistory> paymentData = new ArrayList<>();

    public PaymentHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new HistoryPresenterImpl(this, MyCashClientApplication.getNetworkService());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_history, container, false);

        unbinder = ButterKnife.bind(this, view);

        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.history), false, false);
        // Initialization of RecyclerView
        initRecyclerView();
        initUI();

        if ( ConnectionHelper.isConnected(getContext())) {
            presenter.getPaymentsHistoryData();
        } else {
            showNoConnectionMessage();
        }
        //((NavigationActivity2)getActivity()).historyTabs.setVisibility(ViewPager.VISIBLE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.history), false, false);

        //if(historyList == null)
    }

    private void initUI() {
        // As for fontFamily if you set a default font in Calligraphy then that will always override fontFamily
        // because of it as a solution here we are giving TextView style programmatically
        //dateTextView.setTypeface(dateTextView.getTypeface(), Typeface.BOLD);
        //cardTextView.setTypeface(cardTextView.getTypeface(), Typeface.BOLD);
        //cashTextView.setTypeface(cashTextView.getTypeface(), Typeface.BOLD);
        //fromBonusTextView.setTypeface(fromBonusTextView.getTypeface(), Typeface.BOLD);
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        reposRecyclerView.setLayoutManager(layoutManager);

        historyAdapter = new HistoryAdapter(getActivity(), paymentData);
        historyAdapter.setClickListener(this);
        reposRecyclerView.setAdapter(historyAdapter);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        if(presenter != null)
           presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void showPaymentHistoryData(List<PaymentHistory> paymentData) {
        this.historyList = paymentData;

        reposRecyclerView.setVisibility(View.VISIBLE);
        noDataView.setVisibility(View.GONE);

        this.paymentData.addAll(paymentData);
        historyAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNoResult() {
        reposRecyclerView.setVisibility(View.GONE);
        noDataView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPaymentHistoryResponseError(String error) {
        if( error.isEmpty()) {
            error = getResources().getString(R.string.msg_something_error);
        }
        //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(View view, int position) {
        Fragment fragment = new PaymentHistoryDetailsFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putSerializable(HISTORY_ITEM, historyList.get(position));
        args.putString(PAYMENT_ID, String.valueOf(historyList.get(position).getId()));
        fragment.setArguments(args);

        transaction.setCustomAnimations(R.anim.trans_left_in, R.anim.trans_left_out, R.anim.trans_right_in, R.anim.trans_right_out);
        transaction.add(R.id.history_fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
