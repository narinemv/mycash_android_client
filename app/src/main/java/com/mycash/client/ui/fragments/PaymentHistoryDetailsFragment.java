package com.mycash.client.ui.fragments;

import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mycash.client.MyCashClientApplication;
import com.mycash.client.R;
import com.mycash.client.helper.ConnectionHelper;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.HistoryDetailsPresenterImpl;
import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.model.entity.PaymentHistoryDetails;
import com.mycash.client.presenter.IHistoryDetailsPresenter;
import com.mycash.client.ui.activity.BaseActivity;
import com.mycash.client.ui.view.IHistoryDetailsView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.mycash.client.ui.activity.BaseActivity.HISTORY_ITEM;
import static com.mycash.client.ui.activity.BaseActivity.PAYMENT_ID;

public class PaymentHistoryDetailsFragment extends Fragment implements IHistoryDetailsView{

    @BindView(R.id.details_order_id)
    TextView detailsOrderId;

    @BindView(R.id.details_header)
    LinearLayout detailsHeader;

    @BindView(R.id.details_branch_id)
    TextView detailsBranchId;

    @BindView(R.id.details_bonus)
    TextView detailsBonus;

    @BindView(R.id.details_datetime)
    TextView detailsDatetime;

    @BindView(R.id.details_cash)
    TextView detailsCash;

    @BindView(R.id.details_commition)
    TextView detailsCommition;

    @BindView(R.id.details_from_bonus)
    TextView detailsFromBonus;

    @BindView(R.id.details_branchName)
    TextView detailsBranchName;

    @BindView(R.id.details_address)
    TextView detailsAddress;

    @BindView(R.id.details_bonus_charge)
    TextView detailsBonusCharging;

    @BindView(R.id.details_total_bonus)
    TextView detailsTotalBonus;

    @BindColor(R.color.colorNonValid)
    int colorNonValid;

    @BindColor(R.color.colorAccent)
    int colorValid;

    private PaymentHistory paymentHistoryData;
    private Unbinder unbinder;
    private String paymentId;
    private IHistoryDetailsPresenter presenter;
    private PaymentHistoryDetails details;

    public PaymentHistoryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paymentHistoryData = (PaymentHistory) getArguments().getSerializable(HISTORY_ITEM);
            paymentId = (String) getArguments().get(PAYMENT_ID);
        }

        presenter = new HistoryDetailsPresenterImpl(this, MyCashClientApplication.getNetworkService());

        if ( ConnectionHelper.isConnected(getContext())) {
            presenter.getPaymentHistoryDetails(paymentId);
        } else {
            showNoConnectionMessage();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_details, container, false);

        unbinder = ButterKnife.bind(this, view);

        ((BaseActivity)getActivity()).setupActionBar(getString(R.string.history_details), true, false);

       // detailsBranchName.setTypeface(detailsBranchName.getTypeface(), Typeface.BOLD);

        return view;
    }

    private void setupUI() {
        if (paymentHistoryData != null) {
            detailsOrderId.setText(String.valueOf(paymentHistoryData.getId()));
            detailsBranchId.setText(String.valueOf(paymentHistoryData.getStoreId()));
            detailsBonus.setText(String.valueOf(paymentHistoryData.getBonus()));
            detailsDatetime.setText(Utils.changeDateFormat(paymentHistoryData.getDate()));
            detailsCash.setText(String.valueOf(paymentHistoryData.getCash()));
            detailsCommition.setText(String.valueOf(paymentHistoryData.getCommission()));
            detailsFromBonus.setText(String.valueOf(paymentHistoryData.getFromBonus()));

            if (!paymentHistoryData.getValid()) {
                ((GradientDrawable) detailsHeader.getBackground()).setColor(colorNonValid);
            }else {
                ((GradientDrawable) detailsHeader.getBackground()).setColor(colorValid);
            }

            detailsBranchName.setText(details.getStore().getName());
            detailsAddress.setText(details.getStore().getAddress());
            if( paymentHistoryData.getBonusCharging()){
                detailsBonusCharging.setText(getString(R.string.pos));
            } else {
                detailsBonusCharging.setText(getString(R.string.neg));
            }
            detailsTotalBonus.setText(String.valueOf(details.getTotalBonus()));
        }
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        if(presenter != null)
           presenter.onDestroy();
//        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(HistoryFragment.class.getSimpleName());
//        if (fragment != null) {
//            fragment.onResume();
//        }
        ((BaseActivity)getActivity()).setupActionBar(getResources().getString(R.string.history), false, false);

        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)getActivity()).setupActionBar(getString(R.string.history_details), true, false);
    }

    @Override
    public void showProgress() {
        ((BaseActivity)getActivity())._showProgressBar();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity)getActivity())._hideProgressBar();
    }

    @Override
    public void showPaymentHistoryDetails(PaymentHistoryDetails details) {
        this.details = details;
        setupUI();
    }

    @Override
    public void showPaymentHistoryDetailsResponseError(String error) {
        ((BaseActivity)getActivity())._showError(error);
    }

    @Override
    public void showNoConnectionMessage() {
        Toast.makeText(getActivity(), getResources().getString(R.string.connession_ko), Toast.LENGTH_LONG).show();
    }
}
