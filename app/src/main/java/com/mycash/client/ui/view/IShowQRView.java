package com.mycash.client.ui.view;

import android.graphics.Bitmap;

/**
 * Created by Narine on 06,April,2018
 */
public interface IShowQRView {

    void showProgress();

    void hideProgress();

    void showQRBitmap(Bitmap qrBitmap);
}
