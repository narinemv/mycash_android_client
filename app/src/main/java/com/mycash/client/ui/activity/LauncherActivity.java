package com.mycash.client.ui.activity;

import android.os.Bundle;
import android.os.Handler;

import com.mycash.client.R;
import com.mycash.client.helper.Utils;

/**
 * Created by Narine on 02,March,2018
 */
public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if( Utils.getToken(getApplicationContext()) == null ) {
                    goToActivity(LoginActivity.class);
                }else {
                    goToActivity(NavigationActivity2.class);
                }
            }
        }, 1000);

    }
}
