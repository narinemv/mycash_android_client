package com.mycash.client.ui.view;

import com.mycash.client.model.entity.PaymentHistoryDetails;

/**
 * Created by Narine on 24,June,2018
 */
public interface IForgotPasswordView {

    void showProgress();

    void hideProgress();

    void setEmailEmpty();

    void setEmailNonValid();

    void setFieldsEmpty();

    void setCodeEmpty();

    void setNewPasswordEmpty();

    void setConfirmPasswordEmpty();

    void setPswdNotConfirmed();

    void showChangePassFields(String response);

    void showSendCodeToEmailError(String error);

    void finishChangePassword();

    void showChangePasswordError(String error);

    void showNoConnectionMessage();
}
