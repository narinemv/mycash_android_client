package com.mycash.client.ui.view;

import com.mycash.client.model.entity.PaymentHistory;
import com.mycash.client.model.entity.TransferBonus;

import java.util.List;

/**
 * Created by Narine on 16,April,2018
 */
public interface ITransferHistoryView {

    void showProgress();

    void hideProgress();

    void showTransferHistoryData(List<TransferBonus> paymentData);

    void showNoResult();

    void showTransferHistoryResponseError(String error);

    void showNoConnectionMessage();
}