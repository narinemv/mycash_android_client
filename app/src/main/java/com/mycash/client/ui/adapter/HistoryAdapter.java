package com.mycash.client.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mycash.client.R;
import com.mycash.client.helper.Utils;
import com.mycash.client.model.entity.PaymentHistory;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Narine on 17,April,2018
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private Context context;
    private List<PaymentHistory> detailsList;
    private HistoryAdapter.ItemClickListener clickListener;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public HistoryAdapter(Context context, List<PaymentHistory> detailsList) {
        this.detailsList = detailsList;
        this.context = context;
    }

    @Override
    public HistoryAdapter.HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_item, null);
        return new HistoryAdapter.HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryAdapter.HistoryViewHolder historyViewHolder, int position) {
        PaymentHistory details = detailsList.get(position);

        historyViewHolder.itemDate.setText(Utils.changeDateFormat(details.getDate()));
        historyViewHolder.itemCard.setText(String.valueOf(details.getCard()));
        historyViewHolder.itemCash.setText(String.valueOf(details.getCash()));
        historyViewHolder.itemFromBonus.setText(String.valueOf(details.getFromBonus()));

        if  (!details.getValid()) {
            historyViewHolder.historyItemLn.setBackgroundColor(historyViewHolder.colorNonValid);
        }else {
            historyViewHolder.historyItemLn.setBackgroundColor(historyViewHolder.colorValid);
        }
    }

    @Override
    public int getItemCount() {
        return detailsList == null ? 0 : detailsList.size();
    }

    protected class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.item_date)
        TextView itemDate;

        @BindView(R.id.item_card)
        TextView itemCard;

        @BindView(R.id.item_cash)
        TextView itemCash;

        @BindView(R.id.item_from_bonus)
        TextView itemFromBonus;

        @BindView(R.id.historyItemLn)
        LinearLayout historyItemLn;

        @BindColor(R.color.colorNonValid)
        int colorNonValid;

        @BindColor(R.color.colorDetailsBgColor)
        int colorValid;

        public HistoryViewHolder(View view) {
            super(view);

            // binding view
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public  void setClickListener(HistoryAdapter.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
