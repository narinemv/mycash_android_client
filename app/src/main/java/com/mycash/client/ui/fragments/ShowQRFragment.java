package com.mycash.client.ui.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mycash.client.MyCashClientConstants;
import com.mycash.client.R;
import com.mycash.client.ui.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ShowQRFragment extends Fragment {

    @BindView(R.id.qrImageView)
    ImageView qrImageView;

    private byte[] qrBitmapByteArray;
    private Unbinder unbinder;

    public ShowQRFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            qrBitmapByteArray = getArguments().getByteArray(MyCashClientConstants.QR_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_qr, container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((BaseActivity)getActivity()).setupActionBar("", true, false);

        Bitmap bmp = BitmapFactory.decodeByteArray(qrBitmapByteArray, 0, qrBitmapByteArray.length);
        qrImageView.setImageBitmap(bmp);
    }

    @Override
    public void onDestroyView() {
        if( unbinder != null){
            unbinder.unbind();
        }
        super.onDestroyView();
    }
}
